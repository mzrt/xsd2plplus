### xsdplplus
Генератор plplus-кода для обработки xsd-файла

### Установка/запуск
```
npm install
npm start
```

### Сборка проекта
```
npm install
npm run build
```

### Запуск тестов
```
npm test
```