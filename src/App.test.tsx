import React from "react";
import ReactDOM from "react-dom";
import { TemplatesProps } from "./actions/templates";
import App from "./App";
import { Templates } from "./utils/templates";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <App
      onChangeXsdTbl={(): void => {}}
      onChangeTemplate={<T extends TemplatesProps>(prop: T) => (
        _value: Templates[T]
      ): void => {}}
    />,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
