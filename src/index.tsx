// import "bootstrap/dist/css/bootstrap.min.css";
// import "typeface-roboto";
// These must be the first lines in src/index.js
import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import React from "react";
import ReactDOM from "react-dom";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { mainReducer, defaultState } from "./reducers";
import thunk from "redux-thunk";

import "./index.css";
import App from "./containers/AppContainer";
import * as serviceWorker from "./serviceWorker";
import { ROOT_NODE } from "./constants";

const store = createStore(mainReducer, defaultState(), applyMiddleware(thunk));

const render = (): void => {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    ROOT_NODE as HTMLElement
  );
};
render();
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
