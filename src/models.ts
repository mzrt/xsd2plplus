import JSZip from "jszip";

export class InnerFile {
  constructor(
    public zipEntry: JSZip.JSZipObject,
    public relativePath: string = ""
  ) {}
}
export class FileInfo {
  constructor(
    file: File,
    dateStart: Date = new Date(),
    contentFiles: InnerFile[] = []
  ) {
    this.file = file;
    this.dateStart = dateStart || new Date();
    this.contentFiles = contentFiles || [];
  }
  public file: File;
  public dateStart: Date;
  public dateFinish?: Date;
  public contentFiles: InnerFile[];
  public error: boolean = false;
  public errorMessage: string = "";
}
export interface FileContentInfo {
  filename?: string;
  relativePath?: string;
  status: "unknow" | "loading" | "ready";
  text: string;
  open?: boolean;
}
