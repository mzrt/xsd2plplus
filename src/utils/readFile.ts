import fs from "fs";

export const readModuleFile = (
  path: string,
  callback: (err: NodeJS.ErrnoException | null, words: string) => void
): void => {
  try {
    const filename = require.resolve(path);
    fs.readFile(filename, "utf8", callback);
  } catch (e) {
    callback(e, "");
  }
};
