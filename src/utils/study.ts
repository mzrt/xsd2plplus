export const getRandom = (): number => Math.random() * 100;
export const getMessage = (): string => {
  const rnd = getRandom();
  return rnd + (rnd > 50 ? ">50" : "<=50");
};
