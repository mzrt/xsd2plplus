import { tStr } from "./templates";
import { globaldefaults } from "../constantsDefaultGeneratorSettingsWrapped";
const {
  funcReadString,
  funcWrite,
  funcReadNumber,
  funcReadBoolean,
  funcReadDate,
  funcWriteConvertDate,
  reservWords: reservWordsArr
} = globaldefaults;
export const getReservWordReg = (reservWordsStr: string): RegExp => {
  return new RegExp(
    reservWordsStr
      .split("\n")
      .join("|")
      .replace(/(^)\|+|\|+(\|)(?!$)(?!\|)|\|+($)/g, "$1$2")
      .replace(/\s/g, "")
      .replace(/(^|\|)/g, "$1^")
      .replace(/($|\|)/g, "$$$1"),
    "i"
  );
};
export interface DataTypeTemplates {
  funcReadString: string;
  funcWrite: string;
  funcReadNumber: string;
  funcReadBoolean: string;
  funcReadDate: string;
  funcWriteConvertDate: string;
}
export const defaultReservWords = tStr(reservWordsArr);
export const defaultDataTypeTemplates: DataTypeTemplates = {
  funcReadString: tStr(funcReadString),
  funcWrite: tStr(funcWrite),
  funcReadNumber: tStr(funcReadNumber),
  funcReadBoolean: tStr(funcReadBoolean),
  funcReadDate: tStr(funcReadDate),
  funcWriteConvertDate: tStr(funcWriteConvertDate)
};

export const getDataTypeTemplates = ({
  funcReadString,
  funcWrite,
  funcReadNumber,
  funcReadBoolean,
  funcReadDate,
  funcWriteConvertDate
}: Partial<DataTypeTemplates> = defaultDataTypeTemplates): DataTypeTemplates => {
  return {
    funcReadString: funcReadString || defaultDataTypeTemplates.funcReadString,
    funcWrite: funcWrite || defaultDataTypeTemplates.funcWrite,
    funcReadNumber: funcReadNumber || defaultDataTypeTemplates.funcReadNumber,
    funcReadBoolean:
      funcReadBoolean || defaultDataTypeTemplates.funcReadBoolean,
    funcReadDate: funcReadDate || defaultDataTypeTemplates.funcReadDate,
    funcWriteConvertDate:
      funcWriteConvertDate || defaultDataTypeTemplates.funcWriteConvertDate
  };
};
