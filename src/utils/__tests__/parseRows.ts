import {
  embededTypesOpenCloseDefault,
  embededTypesOpenDefault,
  embededTypesInDefault,
  embededTypesCloseDefault,
  XsdTableHierarchy,
  validateRows,
  XsdTextRow,
  getRegs,
  strToXsdTextRows,
  getStLib,
  PlSimpleType,
  reLib,
  getRow,
  XsdNodePlPlus,
  getPlIdentificator,
  plOptions,
  NodeType,
  RowGroups
} from "../parseRows";
import {
  funcReadString,
  funcWrite
} from "../../constantsDefaultGeneratorSettingsWrapped";
const stLib = getStLib();
it("strToXsdTextRows", (): void => {
  expect(strToXsdTextRows([])).toEqual([]);
  expect(strToXsdTextRows(["str1"])).toEqual([{ rowNum: 1, rowValue: "str1" }]);
  expect(strToXsdTextRows(["str1", "str2"])).toEqual([
    { rowNum: 1, rowValue: "str1" },
    { rowNum: 2, rowValue: "str2" }
  ]);
});

const rows1: XsdTextRow[] = [
  {
    rowNum: 1,
    rowValue: "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]"
  },
  {
    rowNum: 2,
    rowValue: '  \telit\t\txsd:string [maxLength="255"]\tUt enim\t[1]'
  }
];
const rows2: XsdTextRow[] = [
  {
    rowNum: 1,
    rowValue: "  \tameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]"
  },
  {
    rowNum: 2,
    rowValue: '  \telit\t\txsd:string [maxLength="255"]\tUt enim\t[1]'
  }
];
const rows3src = [
  "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
  "  \tconsectetur\t\txsd:date\tut labore et\t[1]",
  "  \tadipisicing\t\txsd:date\tdolore magna aliqua\t[0-1]",
  '  \telit\t\txsd:string [maxLength="255"]\tUt enim\t[1]',
  " ┌\tsed┌choice\t\txsd:double\tad. minim\t[1]",
  " │\tdo\t\txsd:date\tveniam. quis\t[1]",
  " │\teiusmod\t\txsd:boolean\tnostrud. exercitation\t[1]",
  ' │\ttempor\t\txsd:string [maxLength="4000"]\tullamco. laboris\t[1]',
  ' └\tincididunt\t\txsd:string [maxLength="35"]\tnisi ut aliquip.свойства\t[1]'
];
const rows3: XsdTextRow[] = strToXsdTextRows(rows3src);
const rows4src = [
  "\tListOfLorem\t\t\t\t\t\tcommodo consequat lorem\t[0-1]",
  "\t\tIpsum\t\t\t\t\tIpsum lorem\t[1]",
  '\t\t\tLoremIdIPSUM\t\t\txsd:string [pattern="([0-9]|[a-f]|[A-F]){32}"]\tID lorem Duis IPSUM\t[1]',
  '\t\t\tdolore\t\t\txsd:string [maxLength="64"]\taute irure\t[1]',
  "\t\t\tmagna\t\t\txsd:date\tdolor in reprehenderit\t[1]",
  "\t\t\taliqua\t\t\txsd:date\tin voluptate velit esse\t[1]",
  "\t\t\tLorem\t\t\txsd:date\tcillum dolore eu\t[0-1]",
  "\t\t\tipsum\t\t\txsd:date\tfugiat nulla pariatur Excepteur\t[1]",
  "\t\t\tdolor\t\t\txsd:date\tsint occaecat cupidatat\t[0-1]",
  '\t\t\tsit\t\t\txsd:string [maxLength="3"]\tnon proident\t[1]',
  '\t\t\tamet\t\t\txsd:string [maxLength="3"]\tsunt in\t[1]',
  "\t\t\tconsectetur\t\t\t\tculpa qui officia deserunt mollit/ anim\t[0-1]",
  '\t\t\t\tadipisicing\t\txsd:string [maxLength="15"]\tid est, laborum Lorem ipsum dolor sit amet\t[0-1]',
  '\t\t\t\telit\t\txsd:string [maxLength="15"]\tconsectetur adipisicing, elit sed do eiusmod tempor incididunt\t[0-1]'
];
const { regLevelClear } = getRegs();
const levelUp = (ri: string): string => ri.replace(regLevelClear, "");
const rows4: XsdTextRow[] = strToXsdTextRows(rows4src.map(levelUp));
const rows5src = [
  "Lorem ipsum dolor sit amet\t\t\t\t\t\t\t\t",
  "Элемент\t\t\t\t\t\tТип\tОписание\tКратность",
  "DuisOnCreateRq\t\t\t\t\t\t\tExcepteur sint occaecat\t[1]",
  '  \treprehenderit\t\t\t\t\txsd:string [pattern="[0-9a-zA-Z]{32}"]\tfugiat nulla pariatur\t[1]',
  '  \t@attrculpa\t\t\t\t\txsd:string [pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{2}:[0-9]{2}(:[0-9]{2}(\\.[0-9]{3})?((-|\\+)[0-9]{2}:[0-9]{2})?)?)?"]\tLorem ipsum dolor sit amet\t[1]',
  '  \t@attrproident\t\t\t\t\txsd:string [maxLength="36"]\tUt enim ad minim veniam\t[1]',
  '  \ttst2:cillum\t\t\t\t\txsd:string [maxLength="36"]\tExcepteur sint occaecat cupidatat\t[1]',
  " [\tUserInfo┌sequence[0-1]\t\t\t\t\tuinfo\tconsectetur adipisicing elit\t[1]",
  '  \t\tUserIdaliquip\t\t\t\txsd:string [maxLength="100"]\tsed, aliquip ex ea commodo\t[1]',
  '  \t\tUserIdanim\t\t\t\txsd:string [maxLength="100"]\tdo, anim id est laborum\t[0-1]',
  "  \tListOfLoremFs\t\t\t\t\t\tcommodo consequat lorem\t[0-1]",
  "  \t\tIpsum\t\t\t\t\tIpsum lorem\t[1]",
  "  \t\t\ttst:tempor\t\t\txsd:date\tUt enim ad minim\t[0-1]",
  "  \t\t\tameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
  '  \t\t\t  \telit\t\txsd:string [maxLength="255"]\tUt enim\t[1]',
  "  \t\t\t ┌\tsed┌choice\t\t\tad. minim\t[1-N]",
  "  \t\t\t │\t ┌\tnisi┌choice\txsd:date\tsed do eiusmod\t[1]",
  "  \t\t\t │\t │\tut\t\ttempor incididunt\t[1]",
  '  \t\t\t │\t │\t\treprehenderit\txsd:string [maxLength="100"]\tin voluptate\t[0-1]',
  '  \t\t\t │\t │\t\tin\txsd:string [maxLength="25"]\tvelit esse\t[1]',
  "  \t\t\t │\t └\taliquip\txsd:date\tut labore\t[1]",
  "  \t\t\t │\tdo\t\txsd:date\tveniam. quis\t[1]",
  "  \t\t\t │\teiusmod\t\txsd:boolean\tnostrud. exercitation\t[1]",
  '  \t\t\t │\ttempor\t\txsd:string [maxLength="4000"]\tullamco. laboris\t[1]',
  '  \t\t\t └\tincididunt\t\txsd:string [maxLength="35"]\tnisi ut aliquip.свойства\t[1]',
  "  \tUserInfoSnd\t\t\t\t\ttst3:uinfoSnd\tconsectetur adipisicing elit\t[1]",
  '  \t\tUserIdaliquip\t\t\t\txsd:string [maxLength="100"]\tsed, aliquip ex ea commodo\t[1]',
  '  \t\tUserIdanim\t\t\t\txsd:string [maxLength="100"]\tdo, anim id est laborum\t[0-1]'
];
const rows5: XsdTextRow[] = strToXsdTextRows(rows5src);

it("getRegs", (): void => {
  const {
    regChildRow,
    regLevelClear,
    regEmbededTypesOpen,
    regEmbededTypesIn,
    regEmbededTypesClose,
    regEmbededTypesOpenClose,
    levelClear
  } = getRegs();
  [
    "\tameta1\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
    "  \tameta2\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
    "  \t\t\t ┌\tsed┌choice\t\t\tad. minim\t[1-N]",
    " \tameta3\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
    " \t  ameta4\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
    "ameta5\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
    "  ameta6\t\t\t\tdo eiusmod tempor incididunt\t[0-N]"
  ].forEach((rowStr): void => {
    expect(rowStr.search(regEmbededTypesOpen) > -1).toEqual(false);
  });

  [
    "ameta1\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
    "  ameta2\t\t\t\tdo eiusmod tempor incididunt\t[0-N]"
  ].forEach((rowStr): void => {
    expect(rowStr.search(regChildRow) > -1).toEqual(false);
  });
  [
    "\tameta1\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
    "  \tameta2\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
    " \tameta3\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
    " \t  ameta4\t\t\t\tdo eiusmod tempor incididunt\t[0-N]"
  ].forEach((rowStr): void => {
    expect(rowStr.search(regChildRow) > -1).toEqual(true);
  });
  expect(
    "  \tameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]".replace(
      regLevelClear,
      ""
    )
  ).toEqual("ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]");
  expect(
    "  [\tameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]".replace(
      regLevelClear,
      ""
    )
  ).toEqual("ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]");
  expect(levelClear(" ┌\tsed┌choice\t\t\tad. minim\t[1-N]")).toEqual(
    "sed\t\t\tad. minim\t[1-N]"
  );
  expect(
    levelClear(
      " [\tUserInfo┌sequence[0-1]\t\t\t\t\tuinfo\tconsectetur adipisicing elit\t[1]"
    )
  ).toEqual("UserInfo\t\t\t\t\tuinfo\tconsectetur adipisicing elit\t[1]");
  [
    embededTypesOpenCloseDefault,
    embededTypesOpenDefault,
    embededTypesInDefault,
    embededTypesCloseDefault
  ].forEach((embededTypes): void => {
    expect(
      (
        "  " +
        embededTypes +
        "\tameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]"
      ).replace(regLevelClear, "")
    ).toEqual("ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]");
  });
});

it("get_pl_identificator", (): void => {
  const { maxLengthDefine, suffixRec, suffixTbl } = plOptions;
  expect(getPlIdentificator(/any|all/i)()).toEqual("");
  expect(getPlIdentificator(/any|all/i)("")).toEqual("");
  expect(getPlIdentificator(/any|all|clob/i)("cLob")).toEqual('"cLob"');
  expect(getPlIdentificator(/any|all/i)("a" + suffixTbl)).toEqual(
    "a" + suffixTbl
  );
  const maxLengthName = "a" + new Array(maxLengthDefine).join("1");
  expect(getPlIdentificator(/any|all/i)(maxLengthName)).toEqual(maxLengthName);
  const longName = "a" + new Array(maxLengthDefine + 1).join("1");
  expect(longName).toHaveLength(maxLengthDefine + 1);
  expect(getPlIdentificator(/any|all/i)(longName)).not.toEqual(longName);
  expect(getPlIdentificator(/any|all/i)(longName + suffixTbl)).not.toEqual(
    longName + suffixTbl
  );
  expect(getPlIdentificator(/any|all/i)(longName + suffixTbl)).toEqual(
    longName.substr(0, maxLengthDefine - suffixTbl.length) + suffixTbl
  );
  expect(getPlIdentificator(/any|all/i)(longName + suffixRec)).toEqual(
    longName.substr(0, maxLengthDefine - suffixRec.length) + suffixRec
  );
});

it("NodeType", () => {
  expect(new NodeType('xsd:string [maxLength="100"]', stLib)).not.toBeNull();
  expect(new NodeType('xsd:string [maxLength="100"]', stLib)).toEqual({
    name: "xs:string",
    minLength: 0,
    maxLength: 100,
    pattern: "",
    plSimpleType: stLib.get("string"),
    plTypeStr: (stLib.get("string") as PlSimpleType).plName + "(100)"
  } as NodeType);
  expect(new NodeType('xsd:string [pattern="[0-9a-zA-Z]{32}"]', stLib)).toEqual(
    {
      name: "xs:string",
      minLength: 0,
      maxLength: 32,
      pattern: "[0-9a-zA-Z]{32}",
      plSimpleType: stLib.get("string"),
      plTypeStr: (stLib.get("string") as PlSimpleType).plName + "(32)"
    } as NodeType
  );
});

it("getRow", () => {
  {
    const a = getRow(
      "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      0,
      stLib,
      /any/i
    );
    expect(a).not.toBeNull();
    expect(a).toBeInstanceOf(XsdNodePlPlus);
    expect(a).toHaveProperty("type");
    expect(a && a.type.name).toBeUndefined();
    expect(a && a.level).toEqual(0);
    expect(a && a.minOccurs).toEqual(0);
    expect(a && a.maxOccurs).toEqual(Infinity);
    expect(a && a.descr).toEqual("do eiusmod tempor incididunt");
  }
  {
    const a = getRow(
      'UserIdaliquip\t\t\t\txsd:string [maxLength="100"]\tsed, aliquip ex ea commodo\t[1]',
      1,
      stLib,
      /any/i
    );
    expect(a).toHaveProperty("type");
    expect(a && a.type).not.toBeNull();
    expect(a && a.level).toEqual(1);
    expect(a && a.minOccurs).toEqual(1);
    expect(a && a.maxOccurs).toEqual(1);
    expect(a && a.descr).toEqual("sed, aliquip ex ea commodo");
  }
});

it("validateRows", (): void => {
  expect(validateRows(rows1)).toEqual([]);
  expect(validateRows(rows2)).toHaveLength(1);
});
it("getStLib", () => {
  const lStLib = getStLib();
  expect(lStLib).toHaveProperty("get");
  expect(Array.from(lStLib.keys())).toEqual([
    "string",
    "number",
    "integer",
    "boolean",
    "date",
    "choice"
  ]);
  expect(lStLib.get("string")).toBeInstanceOf(PlSimpleType);
  expect(lStLib.get("string")).toEqual(
    new PlSimpleType(
      "string",
      reLib.varhcar,
      "varchar2",
      funcReadString.join("\n"),
      funcWrite.join("\n"),
      ""
    )
  );
  expect(lStLib.get("number")).toBeInstanceOf(PlSimpleType);
  expect(lStLib.get("integer")).toBeInstanceOf(PlSimpleType);
  expect(lStLib.get("boolean")).toBeInstanceOf(PlSimpleType);
  expect(lStLib.get("date")).toBeInstanceOf(PlSimpleType);
  expect(lStLib.get("choice")).toBeInstanceOf(PlSimpleType);
});
it("XsdTableHierarchy", (): void => {
  {
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: rows3
        }
      ],
      0,
      stLib
    );
    const a1 = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: strToXsdTextRows(rows3src.map(levelUp)).slice(1, 4)
        }
      ],
      1,
      stLib
    );
    const a2 = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "choice",
          rows: strToXsdTextRows(rows3src.map(levelUp)).slice(4)
        }
      ],
      1,
      stLib
    );
    expect(a1.childGroup).toHaveLength(1);
    expect(a1.headerRow.struct).toBeUndefined();
    expect(a1.childGroup[0].groupType).toEqual("other");
    expect(a1.childGroup[0].rows).toHaveLength(3);
    expect(a2.childGroup).toHaveLength(1);
    expect(a2.childGroup[0].groupType).toEqual("choice");
    expect(a2.childGroup[0].rows).toHaveLength(5);
    expect(a.childGroup).toHaveLength(1);
    expect(a.childGroup[0].rows).toHaveLength(1);
    expect(a.childGroup[0].rows[0].headerRow.struct).not.toBeUndefined();
    expect(
      a.childGroup[0].rows[0].headerRow.struct &&
        a.childGroup[0].rows[0].headerRow.struct.minOccurs
    ).toEqual(0);
    expect(
      a.childGroup[0].rows[0].headerRow.struct &&
        a.childGroup[0].rows[0].headerRow.struct.maxOccurs
    ).toEqual(Infinity);
    expect(
      a.childGroup[0].rows[0].headerRow.struct &&
        a.childGroup[0].rows[0].headerRow.struct.type.name
    ).toBeUndefined();
    expect(a.childGroup[0].rows[0].childGroup).toHaveLength(2);
    expect(a.childGroup[0].rows[0].childGroup[0].rows).toHaveLength(3);
    expect(a.childGroup[0].rows[0].childGroup[0].groupType).toEqual("other");
    expect(a.childGroup[0].rows[0].childGroup[1].rows).toHaveLength(5);
    expect(a.childGroup[0].rows[0].childGroup[1].groupType).toEqual("choice");
  }
  {
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: rows4
        }
      ],
      0,
      stLib
    );
    expect(a.childGroup).toHaveLength(1);
    expect(a.childGroup[0].rows).toHaveLength(1); //ListOfLorem
    expect(a.childGroup[0].rows[0].childGroup).toHaveLength(1);
    expect(a.childGroup[0].rows[0].childGroup[0].rows).toHaveLength(1); //Ipsum
    expect(
      a.childGroup[0].rows[0].childGroup[0].rows[0].childGroup[0].rows
    ).toHaveLength(10);
    expect(
      a.childGroup[0].rows[0].childGroup[0].rows[0].childGroup[0].rows[9]
        .childGroup[0].rows
    ).toHaveLength(2);
  }
  {
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: rows5
        }
      ],
      0,
      stLib
    );
    expect(a.childGroup[0].rows).toHaveLength(3); //DuisOnCreateRq
    expect(a.childGroup[0].rows[2].childGroup).toHaveLength(1);
    expect(a.childGroup[0].rows[2].childGroup[0].groupType).toEqual("other");
    expect(a.childGroup[0].rows[0].headerRow.struct).toBeUndefined();
    expect(a.childGroup[0].rows[1].headerRow.struct).toBeUndefined();
    expect(
      a.childGroup[0].rows[2].headerRow.struct &&
        a.childGroup[0].rows[2].headerRow.struct.minOccurs
    ).toEqual(1);
    expect(
      a.childGroup[0].rows[2].headerRow.struct &&
        a.childGroup[0].rows[2].headerRow.struct.maxOccurs
    ).toEqual(1);
    expect(
      a.childGroup[0].rows[2].headerRow.struct &&
        a.childGroup[0].rows[2].childGroup[0].rows
    ).toHaveLength(7);
  }
  {
    const rowsStr = [
      "   \t  \tParams\tLimitCalcParam\tПараметры расчета\t[1]",
      "   \t  \t ┌\tOverdraftId┌choice[0-1]\t\tИдентификаторы овердрафта\t[1]",
      "   \t  \t │\t \tEks\t\tПродукт в ЕКС\t[0-1]",
      "   \t  \t │\t \t \tRef\txsd:long\tId объекта\t[1]",
      '   \t  \t │\t \t \tCls\txsd:string [maxLength="32"]\tClass объекта\t[0-1]',
      "   \t  \t │\t \tPprb\t\tПродукт в ППРБ\t[0-1]",
      "   \t  \t │\t \t \tContract\t\tId договора\t[1]",
      "   \t  \t │\t \t \t \tInst\txsd:long\tId инстанса\t[1]",
      "   \t  \t │\t \t \t \tObj\txsd:long\tId объекта\t[1]",
      "   \t  \t │\t \t \t \tPrnt\txsd:long\tId клиента\t[1]",
      "   \t  \t │\t \t \t \tPart\txsd:long\tId партиции\t[1]",
      "   \t  \t │\t \t \tAgreement\txsd:long\tId дополнительного соглашения\t[1]",
      "   \t  \t │\t \t \tCondition\txsd:long\tId условия\t[1]",
      "   \t  \t └\tFirstCalc\tLimitFirstCalc\tПервичный расчет\t[1]",
      '   \t  \t  \t \tUsePeriod\txsd:string [maxLength="16"]\tПериод непрерывного пользования: до 30 дней/до 90 дней (LESS|MORE)\t[0-1]',
      '   \t  \t  \t \tCreditPeriod\txsd:string [maxLength="16"]\tСрок кредитования: до 180 дней/до 1 года (LESS|MORE)\t[0-1]',
      "   \t  \t  \t \tIsCompliance\txsd:boolean\tСоответствие ВНД\t[0-1]",
      "   \t  \t  \t \tActivePeriod\txsd:integer\tПериод действия (дней)\t[0-1]",
      "   \t  \t  \tTurnoverPart\txsd:decimal\tМаксимальный размер доли от кредитового оборота\t[0-1]",
      "   \t  \t  \tContragentPart\txsd:decimal\tМаксимальная доля контрагента\t[0-1]"
    ];
    const a1 = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: strToXsdTextRows(rowsStr.map(levelUp).map(levelUp))
        }
      ],
      1,
      stLib
    );
    expect(a1.childGroup).toHaveLength(1);
    expect(a1.childGroup[0].rows[0].childGroup).toHaveLength(2);
    const gr1 = a1.childGroup[0].rows[0].childGroup[0];
    const gr2 = a1.childGroup[0].rows[0].childGroup[1];
    expect(gr1.rows).toHaveLength(2);
    expect(gr2.rows).toHaveLength(2);
  }
});
it("XsdTableHierarchy getTypeString", (): void => {
  {
    const typesStore = { typesMap: new Map<string, RowGroups[]>() };
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: rows3
        }
      ],
      0,
      stLib
    );
    a.getTypeString(typesStore);
    const keys = Array.from(typesStore.typesMap.keys());
    expect(keys).toHaveLength(2);
    expect(typesStore.typesMap.get(keys[0])).toEqual(
      a.childGroup[0].rows[0].childGroup
    );
    expect(typesStore.typesMap.get(keys[1])).toEqual(a.childGroup);
  }
  {
    const typesStore = { typesMap: new Map<string, RowGroups[]>() };
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: rows4
        }
      ],
      0,
      stLib
    );
    a.getTypeString(typesStore);
    const keys = Array.from(typesStore.typesMap.keys());
    expect(keys).toHaveLength(4);
  }
  {
    const typesStore = { typesMap: new Map<string, RowGroups[]>() };
    const lrows3src = [
      "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      "  \tconsectetur\t\txsd:date\tut labore et\t[1]",
      "ameta2\t\t\t\tdo eiusmod tempor incididunt\t[0-5]",
      "  \tconsectetur\t\txsd:date\tut labore et\t[1]",
      "ameta3\t\t\t\tdo eiusmod tempor incididunt\t[0-5]",
      "  \tconsectetur\t\txsd:date\tut labore et\t[1]"
    ];
    const lrows3: XsdTextRow[] = strToXsdTextRows(lrows3src);
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: lrows3
        }
      ],
      0,
      stLib
    );
    a.getTypeString(typesStore);
    const keys = Array.from(typesStore.typesMap.keys());
    expect(keys).toHaveLength(2);
  }
  {
    const typesStore = { typesMap: new Map<string, RowGroups[]>() };
    const lrows3src = [
      "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      "  \tconsectetur\t\txsd:date\tut labore et\t[1]",
      "ameta2\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      "  \tconsectetur\t\txsd:date\tut labore et\t[1-2]"
    ];
    const lrows3: XsdTextRow[] = strToXsdTextRows(lrows3src);
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: lrows3
        }
      ],
      0,
      stLib
    );
    a.getTypeString(typesStore);
    const keys = Array.from(typesStore.typesMap.keys());
    expect(keys).toHaveLength(3);
  }
  {
    const typesStore = { typesMap: new Map<string, RowGroups[]>() };
    const lrows3src = [
      "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      "  \tconsectetur\t\txsd:date\tut labore et\t[1]",
      "ameta2\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      "  \tconsectetur2\t\txsd:date\tut labore et\t[1]"
    ];
    const lrows3: XsdTextRow[] = strToXsdTextRows(lrows3src);
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: lrows3
        }
      ],
      0,
      stLib
    );
    a.getTypeString(typesStore);
    const keys = Array.from(typesStore.typesMap.keys());
    expect(keys).toHaveLength(3);
  }
  {
    const typesStore = { typesMap: new Map<string, RowGroups[]>() };
    const lrows3src = [
      "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      "  \tconsectetur\t\txsd:date\tut labore et\t[1]",
      "ameta2\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      '  \tconsectetur\t\txsd:string [maxLength="100"]\tut labore et\t[1]'
    ];
    const lrows3: XsdTextRow[] = strToXsdTextRows(lrows3src);
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: lrows3
        }
      ],
      0,
      stLib
    );
    a.getTypeString(typesStore);
    const keys = Array.from(typesStore.typesMap.keys());
    expect(keys).toHaveLength(3);
  }
  {
    const typesStore = { typesMap: new Map<string, RowGroups[]>() };
    const lrows3src = [
      "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      '  \tconsectetur\t\txsd:string [maxLength="101"]\tut labore et\t[1]',
      "ameta2\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      '  \tconsectetur\t\txsd:string [maxLength="100"]\tut labore et\t[1]'
    ];
    const lrows3: XsdTextRow[] = strToXsdTextRows(lrows3src);
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: lrows3
        }
      ],
      0,
      stLib
    );
    a.getTypeString(typesStore);
    const keys = Array.from(typesStore.typesMap.keys());
    expect(keys).toHaveLength(3);
  }
  {
    const typesStore = { typesMap: new Map<string, RowGroups[]>() };
    const lrows3src = [
      "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      '  \tconsectetur\t\txsd:string [maxLength="100"]\tut labore et\t[1]',
      "ameta2\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      '  \tconsectetur\t\txsd:string [maxLength="100"]\tut labore et\t[1]'
    ];
    const lrows3: XsdTextRow[] = strToXsdTextRows(lrows3src);
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: lrows3
        }
      ],
      0,
      stLib
    );
    a.getTypeString(typesStore);
    const keys = Array.from(typesStore.typesMap.keys());
    expect(keys).toHaveLength(2);
  }
});
it("XsdTableHierarchy getTypeString named struct types", () => {
  {
    const typesStore = { typesMap: new Map<string, RowGroups[]>() };
    const lrows3src = [
      "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      '  \tconsectetur\t\txsd:string [maxLength="100"]\tut labore et\t[1]',
      "  \tameta2\t\t\tsomeTypeName\tdo eiusmod tempor incididunt\t[0-N]",
      '  \t\tconsectetur\t\txsd:string [maxLength="100"]\tut labore et\t[1]'
    ];
    const lrows3: XsdTextRow[] = strToXsdTextRows(lrows3src);
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: lrows3
        }
      ],
      0,
      stLib
    );
    a.getTypeString(typesStore);
    const keys = Array.from(typesStore.typesMap.keys());
    expect(keys).toHaveLength(3);

    const ameta = a.childGroup[0].rows[0];
    const ametaStruct = ameta.headerRow.struct;
    const ametaType = ametaStruct && ametaStruct.type;
    expect(ametaStruct && ametaStruct.name).toEqual("ameta");
    expect(ametaType && ametaType.plSimpleType).toBeUndefined();
    expect(ametaType && ametaType.plTypeStr).toBeUndefined();

    const consectetur = ameta.childGroup[0].rows[0];
    const consecteturStruct = consectetur.headerRow.struct;
    const consecteturType = consecteturStruct && consecteturStruct.type;
    expect(consecteturType && consecteturType.name).toEqual("xs:string");
    expect(consecteturType && consecteturType.plSimpleType).toEqual({
      name: "string",
      plName: "varchar2",
      plRead: "&my_get_str_",
      plWrite: "&out_put_val",
      plWriteConv: "",
      re: /(^|[\s\W])string([\s\W]|$)/i
    });
    expect(consecteturType && consecteturType.plTypeStr).toEqual(
      "varchar2(100)"
    );

    const ameta2 = ameta.childGroup[0].rows[1];
    const ameta2Struct = ameta2.headerRow.struct;
    const ameta2Type = ameta2Struct && ameta2Struct.type;
    expect(ameta2Type && ameta2Type.name).toEqual("someTypeName");
    expect(ameta2Type && ameta2Type.plSimpleType).toBeUndefined();
    expect(ameta2Type && ameta2Type.plTypeStr).toBeUndefined();
  }
  {
    const typesStore = { typesMap: new Map<string, RowGroups[]>() };
    const lrows3src = [
      "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      '  \tconsectetur\t\txsd:string [maxLength="100"]\tut labore et\t[1]',
      "  \t\tameta2\t\t\tsomeTypeName\tdo eiusmod tempor incididunt\t[0-N]",
      '  \t\tconsectetur\t\txsd:string [maxLength="100"]\tut labore et\t[1]'
    ];
    const lrows3: XsdTextRow[] = strToXsdTextRows(lrows3src);
    const a = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: lrows3
        }
      ],
      0,
      stLib
    );
    a.getTypeString(typesStore);
    const keys = Array.from(typesStore.typesMap.keys());
    expect(keys).toHaveLength(3);

    const ameta = a.childGroup[0].rows[0];

    const consectetur = ameta.childGroup[0].rows[0];
    const consecteturStruct = consectetur.headerRow.struct;
    const consecteturType = consecteturStruct && consecteturStruct.type;
    expect(consecteturType && consecteturType.name).toEqual("xs:string");
    expect(consecteturType && consecteturType.plSimpleType).toBeUndefined();
    expect(consecteturType && consecteturType.plTypeStr).toBeUndefined();

    const ameta2 = consectetur.childGroup[0].rows[0];
    const ameta2Struct = ameta2.headerRow.struct;
    const ameta2Type = ameta2Struct && ameta2Struct.type;
    expect(ameta2Type && ameta2Type.name).toEqual("someTypeName");
    expect(ameta2Type && ameta2Type.plSimpleType).toBeUndefined();
    expect(ameta2Type && ameta2Type.plTypeStr).toBeUndefined();
  }
});
it("getRow simple", () => {
  expect(
    (
      getRow(
        '  \treprehenderit\t\t\t\t\txsd:string [pattern="[0-9a-zA-Z]{32}"]\tfugiat nulla pariatur\t[1]',
        2,
        stLib,
        /any/
      ) || {}
    ).name
  ).toEqual("reprehenderit");
});
it("getRow attributes", () => {
  expect(
    (
      getRow(
        '  \t@attrproident\t\t\t\t\txsd:string [maxLength="36"]\tUt enim ad minim veniam	[1]',
        2,
        stLib,
        /any/
      ) || {}
    ).name
  ).toEqual("attrproident");
});
it("getRow ':' inside name", () => {
  expect(
    (
      getRow(
        '  \t@attrpr:oi:dent\t\t\t\t\txsd:string [maxLength="36"]\tUt enim ad minim veniam	[1]',
        2,
        stLib,
        /any/
      ) || {}
    ).name
  ).toEqual("attrproident");
});
