import { getMessage } from "../study";
jest.mock("../study", () => {
  const originalModule = jest.requireActual("../study");
  return {
    __esModule: true,
    ...originalModule,
    getRandom: jest.fn().mockReturnValue(10)
  };
});
it("getMessage", () => {
  // expect(getMessage()).toEqual(10 + "<=50");
  new Array(100).forEach(() => {
    expect(getMessage()).toEqual(10 + "<=50");
  });
});
