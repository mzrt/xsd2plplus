import { SrcCodes, getSrc, reStr } from "../PlPlusGenerator";
import { XsdTextRow } from "../parseRows";
import { getTemplates, tStr, defaultTemplates } from "../templates";
import { getDataTypeTemplates } from "../TemplatesDataType";
import { versionScript } from "../../components/ChangeLog";
import { headPlplusGlobal } from "../../constantsDefaultGeneratorSettingsWrapped";
import { GeneratorSettings } from "../../reducers/GeneratorSettings";
import { readModuleFile } from "../readFile";

const globalHeaderTemplate = [
  "/*",
  " * Генератор pl/plus " + versionScript,
  " */"
].concat(headPlplusGlobal);
const globalMinusHeader = (srcCode: string): string => {
  return tStr(srcCode.split("\n").slice(globalHeaderTemplate.length + 1));
};

it("Choice error", (done: () => void) => {
  readModuleFile("./SimpleText.txt", (err: any, words: string): void => {
    const srcCode = new SrcCodes(words);
    expect(globalMinusHeader(srcCode.global())).toMatchSnapshot();
    expect(srcCode.local()).toMatchSnapshot();
    done();
  });
});
