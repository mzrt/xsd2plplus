import { SrcCodes, getSrc, reStr } from "../PlPlusGenerator";
import { XsdTextRow } from "../parseRows";
import { getTemplates, tStr, defaultTemplates } from "../templates";
import { getDataTypeTemplates } from "../TemplatesDataType";
import { versionScript } from "../../components/ChangeLog";
import { headPlplusGlobal } from "../../constantsDefaultGeneratorSettingsWrapped";
import { GeneratorSettings } from "../../reducers/GeneratorSettings";
import { readModuleFile } from "../readFile";

const globalHeaderTemplate = [
  "/*",
  " * Генератор pl/plus " + versionScript,
  " */"
].concat(headPlplusGlobal);
const globalMinusHeader = (srcCode: string): string => {
  return tStr(srcCode.split("\n").slice(globalHeaderTemplate.length + 1));
};
it("getSrc", () => {
  expect(
    getSrc("[plName] [type] [descriptionAsComment]", [
      { re: reStr.plName, val: "reprehenderit" },
      { re: reStr.type, val: "varchar2(32)" },
      {
        re: reStr.descriptionAsComment,
        val: "-- Excepteur sint occaecat"
      }
    ])
  ).toEqual("reprehenderit varchar2(32) -- Excepteur sint occaecat");
  expect(
    getSrc("\t[content]", [{ re: reStr.content, val: "reprehenderit" }])
  ).toEqual("\treprehenderit");
  expect(
    getSrc("\t[content]", [{ re: reStr.content, val: "rep\nreh\nenderit" }])
  ).toEqual("\trep\n\treh\n\tenderit");
  expect(
    getSrc("\t[content]", [{ re: reStr.content, val: "rep\nreh\nenderit\n" }])
  ).toEqual("\trep\n\treh\n\tenderit\n");
  expect(
    getSrc("\t[content]", [
      { re: reStr.content, val: "rep\nreh\n\n\nenderit\n" }
    ])
  ).toEqual("\trep\n\treh\n\n\n\tenderit\n");
});
const templates = getTemplates();
const dataTypeTemplates = getDataTypeTemplates();
it("ScrCodes constructor", () => {
  const hrRoot: XsdTextRow = {
    rowNum: 0,
    rowValue: ""
  };
  expect(new SrcCodes("", templates, dataTypeTemplates)).toHaveProperty(
    "typesDefine"
  );
  expect(
    new SrcCodes("", templates, dataTypeTemplates).typesDefine.size
  ).toEqual(0);
  {
    const srcCode = new SrcCodes(
      'elit\t\txsd:string [maxLength="255"]\tUt enim\t[1]'
    );
    expect(srcCode).toHaveProperty("typesDefine");
    expect(srcCode.typesDefine.size).toEqual(0);
  }
});
it("ScrCodes src generate", () => {
  const srcCode = new SrcCodes(
    tStr([
      "DuisOnCreateRq\t\t\t\t\t\t\tExcepteur sint occaecat\t[1]",
      '  \treprehenderit\t\t\t\t\txsd:string [pattern="[0-9a-zA-Z]{32}"]\tfugiat nulla pariatur\t[1]'
    ])
  );
  expect(globalMinusHeader(srcCode.global())).toMatchSnapshot();
});

it("ScrCodes src generate choice", () => {
  const srcCode = new SrcCodes(
    tStr([
      "ameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      '  \telit\t\txsd:string [maxLength="255"]\tUt enim\t[1]',
      " ┌\tsed┌choice\t\t\tad. minim\t[1-N]",
      " │\t ┌\tnisi┌choice\txsd:date\tsed do eiusmod\t[1]",
      " │\t │\tut\t\ttempor incididunt\t[1]",
      ' │\t │\t\treprehenderit\txsd:string [maxLength="100"]\tin voluptate\t[0-1]',
      ' │\t │\t\tin\txsd:string [maxLength="25"]\tvelit esse\t[1]',
      " │\t └\taliquip\txsd:date\tut labore\t[1]",
      " │\tdo\t\txsd:date\tveniam. quis\t[1]",
      " │\teiusmod\t\txsd:boolean\tnostrud. exercitation\t[1]",
      ' │\ttempor\t\txsd:string [maxLength="4000"]\tullamco. laboris\t[1]',
      ' └\tincididunt\t\txsd:string [maxLength="35"]\tnisi ut aliquip.свойства\t[1]'
    ])
  );
  expect(globalMinusHeader(srcCode.global())).toMatchSnapshot();
});
it("SrcCode named type", () => {
  const srcCode = new SrcCodes(
    tStr([
      "Lorem ipsum dolor sit amet\t\t\t\t\t\t\t\t",
      "Элемент\t\t\t\t\t\tТип\tОписание\tКратность",
      "DuisOnCreateRq\t\t\t\t\t\tsomeType\tExcepteur sint occaecat\t[1]",
      '  \treprehenderit\t\t\t\t\txsd:string [pattern="[0-9a-zA-Z]{32}"]\tfugiat nulla pariatur\t[1]',
      '  \t@attrculpa\t\t\t\t\txsd:string [pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{2}:[0-9]{2}(:[0-9]{2}(.[0-9]{3})?((-|+)[0-9]{2}:[0-9]{2})?)?)?"]\tLorem ipsum dolor sit amet\t[1]',
      '  \t@attrproident\t\t\t\t\txsd:string [maxLength="36"]\tUt enim ad minim veniam\t[1]',
      '  \ttst2:cillum\t\t\t\t\txsd:string [maxLength="36"]\tExcepteur sint occaecat cupidatat\t[1]'
    ])
  );
  expect(globalMinusHeader(srcCode.global())).toMatchSnapshot();
});

it("SrcCode local", () => {
  const srcCode = new SrcCodes(
    tStr([
      "Ipsum\t\t\t\t\tIpsum lorem\t[1]",
      '\tLoremIdIPSUM\t\t\txsd:string [pattern="([0-9]|[a-f]|[A-F]){32}"]\tID lorem Duis IPSUM\t[1]',
      "\texa\t\t\t\tculpa qui officia deserunt\t[0-N]",
      '\t\tea\t\txsd:string [maxLength="100"]\tmollit anim id\t[1]',
      "\t\tDuis\t\txsd:date\test laborum Lorem\t[1]",
      "\t\taute\t\txsd:date\tipsum dolor sit\t[1]",
      '\t\tirure\t\txsd:string [maxLength="30"]\tamet consectetur adipisicing (день/elit/sed)\t[0-1]',
      "\t\tdolor\t\txsd:integer\tdo eiusmod tempor\t[0-1]",
      '\t\tin\t\txsd:string [maxLength="1"]\tincididunt: 1 – ut; 2 – labore et\t[0-1]',
      "\t\treprehenderit\t\txsd:date\tdolore magna aliqua\t[1]",
      '\t\tin2\t\txsd:string [maxLength="1"]\tUt enim ad minim: 1 – veniam quis nostrud exercitation ullamco, 2 – laboris nisi ut aliquip, 3 – ex ea, 4– commodo consequat Duis aute irure dolor in\t[0-1]',
      "\t\tvoluptate\t\txsd:date\treprehenderit in voluptate\t[0-1]"
    ])
  );
  expect(srcCode.local()).toMatchSnapshot();
});

it("SrcCode global local choice", () => {
  const srcCode = new SrcCodes(
    tStr([
      "Ipsum\t\t\t\t\tIpsum lorem\t[1]",
      "\ttst:tempor\t\t\txsd:date\tUt enim ad minim\t[0-1]",
      "\tameta\t\t\t\tdo eiusmod tempor incididunt\t[0-N]",
      '\t  \telit\t\txsd:string [maxLength="255"]\tUt enim\t[1]',
      "\t ┌\tsed┌choice\t\t\tad. minim\t[1-N]",
      "\t │\t ┌\tnisi┌choice\txsd:date\tsed do eiusmod\t[1]",
      "\t │\t │\tut\t\ttempor incididunt\t[1]",
      '\t │\t │\t\treprehenderit\txsd:string [maxLength="100"]\tin voluptate\t[0-1]',
      '\t │\t │\t\tin\txsd:string [maxLength="25"]\tvelit esse\t[1]',
      "\t │\t └\taliquip\txsd:date\tut labore\t[1]",
      "\t │\tdo\t\txsd:date\tveniam. quis\t[1]",
      "\t │\teiusmod\t\txsd:boolean\tnostrud. exercitation\t[1]",
      '\t │\ttempor\t\txsd:string [maxLength="4000"]\tullamco. laboris\t[1]',
      '\t └\tincididunt\t\txsd:string [maxLength="35"]\tnisi ut aliquip.свойства\t[1]'
    ])
  );
  expect(srcCode.local()).toMatchSnapshot();
  expect(globalMinusHeader(srcCode.global())).toMatchSnapshot();
});
it("check empty", () => {
  const srcCode = new SrcCodes(
    tStr([
      "Lorem ipsum dolor sit amet\t\t\t\t\t\t\t\t",
      "Элемент\t\t\t\t\t\tТип\tОписание\tКратность",
      "DuisOnCreateRq\t\t\t\t\t\t\tExcepteur sint occaecat\t[1]",
      '  \treprehenderit\t\t\t\t\txsd:string [pattern="[0-9a-zA-Z]{32}"]\tfugiat nulla pariatur\t[1]',
      " [\tUserInfo┌sequence[0-1]\t\t\t\t\tuinfo\tconsectetur adipisicing elit\t[1]",
      '  \t\tUserIdaliquip\t\t\t\txsd:string [maxLength="100"]\tsed, aliquip ex ea commodo\t[1]',
      '  \t\tUserIdanim\t\t\t\txsd:string [maxLength="100"]\tdo, anim id est laborum\t[0-1]',
      " [\tUserInfo2┌sequence[0-1]\t\t\t\t\tuinfo\tconsectetur adipisicing elit\t[0-1]",
      "  \t\tIpsum\t\t\t\t\tIpsum lorem\t[1]",
      '  \t\t\tLoremIdIPSUM\t\t\txsd:string [pattern="([0-9]|[a-f]|[A-F]){32}"]\tID lorem Duis IPSUM\t[1]',
      "  \t\t\tconsectetur\t\t\t\tculpa qui officia deserunt mollit/ anim\t[0-1]",
      '  \t\t\t\tadipisicing\t\txsd:string [maxLength="15"]\tid est, laborum Lorem ipsum dolor sit amet\t[0-1]',
      '  \t\t\t\telit\t\txsd:string [maxLength="15"]\tconsectetur adipisicing, elit sed do eiusmod tempor incididunt\t[0-1]'
    ])
  );
  expect(globalMinusHeader(srcCode.global())).toMatchSnapshot();
  expect(srcCode.local()).toMatchSnapshot();
});
it("default xsd", () => {
  const srcCode = new SrcCodes(new GeneratorSettings().tbl);
  expect(globalMinusHeader(srcCode.global())).toMatchSnapshot();
  expect(srcCode.local()).toMatchSnapshot();
});

it("extended xsd", (done: () => void) => {
  readModuleFile("./Example.txt", (err: any, words: string): void => {
    const srcCode = new SrcCodes(words);
    expect(globalMinusHeader(srcCode.global())).toMatchSnapshot();
    expect(srcCode.local()).toMatchSnapshot();
    done();
  });
});
it("LongNames", (done: () => void) => {
  readModuleFile("./TestLongNames.txt", (err: any, words: string): void => {
    const srcCode = new SrcCodes(words);
    expect(globalMinusHeader(srcCode.global())).toMatchSnapshot();
    expect(srcCode.local()).toMatchSnapshot();
    done();
  });
});
