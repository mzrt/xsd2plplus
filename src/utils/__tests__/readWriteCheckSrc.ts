import {
  GroupTypeNames,
  readPlElementTest,
  readWriteCheckSrc
} from "../PlPlusGenerator";
import { getStLib, XsdNodePlPlus } from "../parseRows";
import { defaultTemplates } from "../templates";
import { readPlElement } from "../PlPropsCode";
jest.mock("../PlPropsCode");
const stLib = getStLib();

it("readWriteCheckSrc", () => {
  expect(
    readPlElement(
      new XsdNodePlPlus(0, "", "", "", 0, 0, stLib, /any/),
      undefined,
      true,
      defaultTemplates
    )
  ).toEqual("readPl\n");
  expect(
    readPlElementTest(
      new XsdNodePlPlus(0, "", "", "", 0, 0, stLib, /any/),
      undefined,
      true,
      defaultTemplates
    )
  ).toEqual("readPl\n");
  {
    const children: GroupTypeNames[] = [];
    expect(
      readWriteCheckSrc(
        children,
        defaultTemplates,
        jest.fn(),
        (name: string) => {}
      )
    ).toEqual({
      readCode: "",
      writeCode: "",
      checkCode: ""
    });
  }
  {
    const children: GroupTypeNames[] = [
      {
        groupType: "other",
        rows: [
          {
            struct: new XsdNodePlPlus(0, "", "", "", 0, 0, stLib, /any/),
            typeName: "1"
          },
          {
            struct: new XsdNodePlPlus(0, "", "", "", 0, 0, stLib, /any/),
            typeName: "2"
          }
        ]
      },
      {
        groupType: "other",
        rows: []
      },
      {
        groupType: "choice",
        rows: [
          {
            struct: new XsdNodePlPlus(0, "", "", "", 0, 0, stLib, /any/),
            typeName: "1"
          },
          {
            struct: new XsdNodePlPlus(0, "", "", "", 0, 0, stLib, /any/),
            typeName: "2"
          }
        ]
      }
    ];
    expect(
      readWriteCheckSrc(
        children,
        defaultTemplates,
        jest.fn(),
        (name: string) => {}
      )
    ).toEqual({
      readCode: "readPl\nreadPl\nreadPl\nreadPl\n",
      writeCode: "writePl\nwritePl\nwritePl\nwritePl\n",
      checkCode: "checkPl\ncheckPl\ncheckPl\ncheckPl"
    });
  }
});
