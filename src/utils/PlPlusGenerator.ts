import {
  XsdNodePlPlus,
  XsdTableHierarchy,
  getPlIdentificator,
  getReadFuncName,
  getWriteFuncName,
  getCheckFuncName,
  suffixRec,
  suffixTbl,
  getStLib,
  StLib,
  strToXsdTextRows,
  RowGroups,
  GroupType
} from "./parseRows";
import { versionScript } from "../components/ChangeLog";
import {
  getDataTypeTemplates,
  DataTypeTemplates,
  getReservWordReg,
  defaultReservWords,
  defaultDataTypeTemplates
} from "./TemplatesDataType";
import { Templates, defaultTemplates } from "./templates";
import { ApplyMaskState, defaultMask } from "../components/GeneratorOptions";
import { readPlElement, writePlElement, checkPlElement } from "./PlPropsCode";

const stringHashCode = (str: string): number => {
  let hash: number = 0,
    i,
    chr: number;
  if (str.length === 0) return hash;
  for (i = 0; i < str.length; i++) {
    chr = str.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; //convert to 32bit integer
  }
  return hash;
};
//
type RegStrKeys =
  | "tagName"
  | "tagTypeName"
  | "type"
  | "content"
  | "typeTbl"
  | "functReadName"
  | "functWriteName"
  | "functCheckemptyName"
  | "functCheckemptyTblName"
  | "plName"
  | "typeItem"
  | "readSimpleTypeName"
  | "writeSimpleTypeName"
  | "description"
  | "descriptionAsComment"
  | "level"
  | "contentCheck";
interface RegRef {
  rstr: string;
}
type RegStrRecord = Record<RegStrKeys, RegRef>;
export const reStr: RegStrRecord = {
  tagName: { rstr: "\\[tagName\\]" },
  tagTypeName: { rstr: "\\[tagTypeName\\]" },
  type: { rstr: "\\[type\\]" },
  content: { rstr: "\\[content\\]" },
  typeTbl: { rstr: "\\[typeTbl\\]" },
  functReadName: { rstr: "\\[functReadName\\]" },
  functWriteName: { rstr: "\\[functWriteName\\]" },
  functCheckemptyName: { rstr: "\\[functCheckemptyName\\]" },
  functCheckemptyTblName: { rstr: "\\[functCheckEmptyTblName\\]" },
  plName: { rstr: "\\[plName\\]" },
  typeItem: { rstr: "\\[typeItem\\]" },
  readSimpleTypeName: { rstr: "\\[readSimpleTypeName\\]" },
  writeSimpleTypeName: { rstr: "\\[writeSimpleTypeName\\]" },
  description: { rstr: "\\[description\\]" },
  descriptionAsComment: { rstr: "\\[descriptionAsComment\\]" },
  level: { rstr: "\\[УровеньВложенности=\\d+\\]" },
  contentCheck: { rstr: "\\[contentCheck\\]" }
};
export const rgIdentificator = new RegExp("^[A-Z]\\w+$", "i");
export interface ReplaceParam {
  re: RegRef; // Строка регулярного выражения
  val: string;
}
// Функция возвращает текст полученный путем подмены меток на их значения(массив replace хранит объекты с парой re, val ) в шаблоне srcTemplate
export function getSrc(srcTemplate: string, replace: ReplaceParam[]): string {
  let retVal = srcTemplate;
  if (retVal.length > 0) {
    retVal = replace.reduce(function (code, rp) {
      let prefix;
      let curVal = rp.val;
      let retVal = code || "";
      const divider = ";|;";
      let lRe;
      if (rp.re === reStr.content) {
        prefix = code
          .split("\n")
          .join("||")
          .replace(
            new RegExp(
              ".*(\\|\\||^)((\\t|\\s)*)" + rp.re.rstr + ".*|.*",
              "gim"
            ),
            "$2"
          );
        if (prefix.length > 0)
          curVal = curVal.replace(
            new RegExp("(\\n)(?!$)", "gim"),
            "$1" + prefix
          );
      }
      if (rp.re === reStr.level) {
        lRe = new RegExp(
          rp.re.rstr.replace(/=\\d\+/g, "=" + rp.val) +
            "{(.*)}|" +
            rp.re.rstr +
            "\\{.*\\}",
          "gim"
        );
        retVal = retVal
          .split("\n")
          .join(divider)
          .replace(lRe, "$1")
          .split(divider)
          .join("\n");
      } else {
        retVal = retVal.replace(new RegExp(rp.re.rstr, "gim"), curVal);
      }
      return retVal;
    }, retVal);
  }
  return retVal;
}
export interface StructTypeName {
  struct: XsdNodePlPlus;
  typeName: string;
}
export interface GroupTypeNames {
  groupType: GroupType;
  rows: StructTypeName[];
}
// Генератор кода для определения типа
export function readWriteCheckSrc(
  children: GroupTypeNames[],
  templates: Templates,
  getTypeByName: (name: string) => PlDefine | undefined,
  onCountChecks: (checkFuncName: string) => void
): { readCode: string; writeCode: string; checkCode: string } {
  const readWritCheckItems = children
    .map((grItem) => {
      if (!grItem.rows.length) {
        return null;
      }
      return grItem.rows.map((childItem) => ({
        read: readPlElement(
          childItem.struct,
          getTypeByName(childItem.typeName),
          grItem.groupType === "choice",
          templates
        ),
        write: writePlElement(
          childItem.struct,
          getTypeByName(childItem.typeName),
          grItem.groupType === "choice",
          templates,
          onCountChecks
        ),
        check: checkPlElement(
          childItem.struct,
          getTypeByName(childItem.typeName),
          grItem.groupType === "choice"
        )
      }));
    })
    .filter((grStr) => grStr && grStr.length);
  const defaultRetVal = { readCode: "", writeCode: "", checkCode: "" };
  return readWritCheckItems && readWritCheckItems.length
    ? {
        readCode: readWritCheckItems
          .map((grItems) => (grItems || []).map(({ read }) => read).join(""))
          .join(""),
        writeCode: readWritCheckItems
          .map((grItems) => (grItems || []).map(({ write }) => write).join(""))
          .join(""),
        checkCode: readWritCheckItems
          .map((grItems) =>
            (grItems || []).map(({ check }) => check).join("\n")
          )
          .join("\n")
      }
    : defaultRetVal;
}
// Генератор кода для чтения тега
export const readPlElementTest = (
  curStruct: XsdNodePlPlus,
  typeStruct: PlDefine | undefined,
  isChoice: boolean,
  template: Templates
): string => {
  return readPlElement(curStruct, typeStruct, isChoice, template);
};
// Генератор кода для определения типа
export function defineTypeSrc(
  children: GroupTypeNames[],
  { structItem }: Templates
): string {
  return children
    .map((grItem) => {
      if (!grItem.rows.length) {
        return "";
      }
      return (
        (grItem.groupType === "choice" ? "choice varchar2(32)\n, " : "") +
        grItem.rows
          .map((childItem) =>
            getSrc(structItem, [
              { re: reStr.plName, val: childItem.struct.plName },
              { re: reStr.type, val: childItem.typeName },
              {
                re: reStr.descriptionAsComment,
                val: childItem.struct.descrAsComment()
              }
            ])
          )
          .join("\n, ")
      );
    })
    .filter((grStr) => Boolean(grStr))
    .join("\n, ");
}
export type PlSection = keyof PlDefine;
export class PlDefine {
  public checkFunct: string = "";
  public depencyTypes: string[] = [];
  public fullname: string;
  public hash: string;
  public hashItem?: string;
  public typeName: string;
  public functCheckemptyName: string = "";
  public functReadName: string = "";
  public functWriteName: string = "";
  public included: boolean = false;
  public name: string;
  public readFunct: string = "";
  public readFunctGlobalDef: string = "";
  public tbl?: PlDefine;
  public typeDefine: string = "";
  public writeFunctGlobalDef: string = "";
  public writeFunct: string = "";
  public constructor(
    name: string,
    fullname: string,
    typeName: string,
    functReadName: string = "",
    functWriteName: string = "",
    readFunctGlobalDef: string = "",
    writeFunctGlobalDef: string = "",
    readFunct: string = "",
    writeFunct: string = "",
    hash: string,
    hashItem?: string,
    public refTd?: PlDefine
  ) {
    this.name = name;
    this.fullname = fullname;
    this.typeName = typeName;
    this.functReadName = functReadName;
    this.functWriteName = functWriteName;
    this.readFunctGlobalDef = readFunctGlobalDef;
    this.writeFunctGlobalDef = writeFunctGlobalDef;
    this.readFunct = readFunct;
    this.writeFunct = writeFunct;
    this.hash = hash;
    this.hashItem = hashItem;
  }
}
// возвращает заголовок функции/процедуры для использования в секции глобальных объявлений
export function getPlFuncHead(template: string): string {
  return (
    template
      .split("\n")
      .join("~~")
      .replace(
        /^(.*(function|procedure).+)(\))(\s|\t|~~)*(return(\s|\t|~~)+.+)*(\s|\t|~~)+is(\s|\t|~~).*|.*/i,
        "$1$3$5"
      )
      .split("~~")
      .join("\n") + ";"
  );
}
function tdFillFuncDefine(
  td: PlDefine,
  replaceHeadTypeParams: ReplaceParam[],
  lGenRead: boolean,
  lGenWrite: boolean,
  readCode: string,
  writeCode: string,
  checkNull: string,
  orderIdx: number,
  vCurrentLevel: number,
  shortBody: boolean,
  {
    procRootRead,
    procRootReadS,
    procRootReadHead,
    procRead,
    procReadHead,
    procRootWrite,
    procRootWriteS,
    procRootWriteHead,
    procWrite,
    procWriteHead,
    procCheckempty,
    procCheckemptyS
  }: Templates
): void {
  let vTmp;
  // сформируем исходный код операций чтения и записи
  const replaceParams = replaceHeadTypeParams.concat([
    { re: reStr.functReadName, val: td.functReadName },
    { re: reStr.functWriteName, val: td.functWriteName },
    { re: reStr.type, val: td.name }
  ]);

  if (lGenRead && (td.readFunct || "").length === 0) {
    // Операция чтения
    if (orderIdx === 0) {
      // По шаблону корневого тега
      vTmp = procRootRead;
      if (shortBody) vTmp = procRootReadS;
      td.readFunct = getSrc(
        vTmp,
        replaceParams.concat({ re: reStr.content, val: readCode })
      );
      td.readFunctGlobalDef = getSrc(procRootReadHead, replaceParams);
    } else {
      // По шаблону вложенных тегов
      td.readFunct = getSrc(
        procRead,
        replaceParams.concat({ re: reStr.content, val: readCode })
      );
      td.readFunctGlobalDef = getSrc(procReadHead, replaceParams);
    }
  }
  if (lGenWrite && (td.writeFunct || "").length === 0) {
    // Операция записи
    if (orderIdx === 0) {
      // По шаблону корневого тега
      vTmp = procRootWrite;
      if (shortBody) vTmp = procRootWriteS;
      td.writeFunct = getSrc(
        vTmp,
        replaceParams.concat(
          { re: reStr.content, val: writeCode },
          { re: reStr.level, val: String(vCurrentLevel) }
        )
      );
      td.writeFunctGlobalDef = getSrc(procRootWriteHead, replaceParams);
    } else {
      // По шаблону вложенных тегов
      td.writeFunct = getSrc(
        procWrite,
        replaceParams.concat(
          { re: reStr.content, val: writeCode },
          { re: reStr.level, val: String(vCurrentLevel) }
        )
      );
      td.writeFunctGlobalDef = getSrc(procWriteHead, replaceParams);
    }
  }
  vTmp = procCheckempty;
  if (shortBody) vTmp = procCheckemptyS;
  td.checkFunct = getSrc(
    vTmp,
    replaceParams.concat(
      { re: reStr.content, val: checkNull },
      { re: reStr.functCheckemptyName, val: td.functCheckemptyName }
    )
  );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////// Структура для хранения самих типов /////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////// и для формирования исходного кода  /////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
export class SrcCodes {
  // формирование операции проверки, включая операции проверки для тех типов от которых зависит текущий
  public getCheckTxt = (types: PlDefine[]): string[] => {
    return types.reduce((txt: string[], type): string[] => {
      let check0;
      if (!type.included) {
        try {
          if (type.depencyTypes && type.depencyTypes.length) {
            const depency: PlDefine[] = [];
            type.depencyTypes.forEach((typeName) => {
              const td = this.getTypeByName(typeName);
              if (td) {
                depency.push(td);
              }
            });
            check0 = this.getCheckTxt(depency);
          }
        } catch (err) {
          debugger;
        }

        type.included = true;
        const check1 = [type["checkFunct"]];
        if (!!check1) {
          txt = check1.concat(txt);
        }
        if (!!check0) {
          txt = check0.concat(txt);
        }
      }
      return txt;
    }, []);
  };
  public levelSrcTxt = (types: PlDefine[], section: PlSection): string[] => {
    return types.reduce((txt: string[], type: PlDefine): string[] => {
      if (section in type && type[section]) {
        txt = txt.concat(String(type[section]));
      }
      if (type.tbl && type.tbl[section])
        txt = txt.concat(String(type.tbl[section]));
      if (
        txt.length > 0 &&
        section === "writeFunct" &&
        !type.included &&
        this.usedChecks.get(type.functCheckemptyName)
      ) {
        txt = txt.concat(this.getCheckTxt([type]));
      }
      return txt;
    }, []);
  };
  public levelsSrcTxt = (
    levels: Array<Array<PlDefine>>,
    sections: PlSection[],
    commentSection: string
  ): string[] => {
    let retVal = levels.reduce((srcText: string[], types): string[] => {
      srcText = srcText.concat(
        sections.reduce((txt: string[], section): string[] => {
          txt = txt.concat(this.levelSrcTxt(types, section));
          return txt;
        }, [])
      );
      return srcText;
    }, []);
    if (sections.length && retVal.length) {
      retVal = [commentSection].concat(retVal);
    } else {
      retVal = [];
    }
    return retVal;
  };

  // Сохранение определений типов по уникальными именами, без дублирования определений типов
  public definePlType = (
    name: string,
    typeDefine: string,
    readFunct: boolean,
    writeFunct: boolean,
    fullname: string,
    typeName: string,
    hash: string,
    hashItem: string
  ): {
    typeName: string | undefined;
    plType: PlDefine | undefined;
    isNew: boolean;
  } => {
    let srcTypeName,
      keyTypeName = name.toLowerCase();
    let vidx;
    let lFunctCheckemptyName;
    let itemTypeName = "",
      itemType: PlDefine | undefined;
    let isNew = false;
    if (hash) {
      // Ищем по хэшу, если нашли, то имя типа берем из найденного типа
      if (this.namesByHash.get(hash)) {
        srcTypeName = this.namesByHash.get(hash) || "";
        keyTypeName = srcTypeName.toLowerCase();
      } else {
        isNew = true;
        // не нашли, проверяем используется ли данное имя уже каким-либо типом
        // если не используется, то оставляем существующее, счетчик дублирующихся имен выставляем в 0
        if (!this.typesDefine.get(keyTypeName)) {
          srcTypeName = name;
          this.typesNamesUniq.set(keyTypeName, 0);
        } else {
          // имя уже используется
          // необходимо добавить порядковый номер для уникальности имени типа
          vidx = (this.typesNamesUniq.get(keyTypeName) || 0) + 1;
          this.typesNamesUniq.set(keyTypeName, vidx);
          srcTypeName = this.getPlIdentificator(name, vidx);
          keyTypeName = srcTypeName.toLowerCase();
          // Проставим имя типа и имя тега
          if (!(keyTypeName in this.typesDefine) && !!hashItem) {
            lFunctCheckemptyName = this.getPlIdentificator("c_" + name, vidx);
          }
        }
      }
      const lTypeDefine: PlDefine = {
        name: "",
        functCheckemptyName: "",
        included: false,
        checkFunct: "",
        depencyTypes: [],
        fullname: fullname,
        typeName: typeName,
        typeDefine: "",
        functReadName: "",
        functWriteName: "",
        readFunctGlobalDef: "",
        writeFunctGlobalDef: "",
        readFunct: "",
        writeFunct: "",
        hash: hash,
        hashItem: hashItem
      };
      if (srcTypeName && !this.typesDefine.get(keyTypeName)) {
        // Проставим имя типа и имя тега
        itemType = !!hashItem ? this.getTypeByHash(hashItem) : undefined;
        if (itemType) {
          // Для табличного типа, операции чтения и записи берем с основного типа
          ({
            name: itemTypeName,
            functReadName: lTypeDefine.functReadName,
            functWriteName: lTypeDefine.functWriteName
          } = itemType);
          lTypeDefine.functCheckemptyName = "";
          // if (!!itemType.functWriteName){
          lTypeDefine.functCheckemptyName =
            lFunctCheckemptyName || this.getPlIdentificator("c_" + srcTypeName);
          // }
        } else {
          // Для не табличного типа, имена операций генерируем из имени типа
          if (readFunct) {
            lTypeDefine.functReadName = this.getReadFuncName(srcTypeName);
          }
          if (writeFunct) {
            lTypeDefine.functWriteName = this.getWriteFuncName(srcTypeName);
          }
          lTypeDefine.functCheckemptyName = this.getCheckFuncName(srcTypeName);
        }
        // сформируем определение типа
        lTypeDefine.typeDefine = getSrc(typeDefine, [
          { re: reStr.type, val: srcTypeName },
          { re: reStr.tagName, val: lTypeDefine.fullname },
          {
            re: reStr.tagTypeName,
            val:
              lTypeDefine.typeName && rgIdentificator.test(lTypeDefine.typeName)
                ? lTypeDefine.typeName
                : lTypeDefine.fullname
          },
          { re: reStr.typeItem, val: itemTypeName }
        ]);
        lTypeDefine.name = srcTypeName;
        this.typesDefine.set(keyTypeName, lTypeDefine);
        this.namesByHash.set(hash, srcTypeName);
      }
    }
    return {
      typeName: srcTypeName,
      plType: this.typesDefine.get(keyTypeName),
      isNew
    };
  };
  // рекурсивная функция, выполняет обход описаний реквизитов,
  // для вложенных реквизитов вызывает саму себя для формирования типа на основе элементов вложенного типа
  // формирует текст объявления структуры, текст процедуры чтения/записи
  // имя созданного типа возвращается в объекте outVals - outVals.typeName
  public getPlDefine = ({
    struct,
    genRead,
    genWrite
  }: {
    struct: XsdTableHierarchy;
    genRead: boolean;
    genWrite: boolean;
  }): string | undefined => {
    const curStruct = struct.headerRow.struct;
    if (!curStruct) {
      return;
    }
    let retVal: string | undefined;
    const depencyTypes: string[] = [];
    const validTypeName =
      curStruct.type &&
      curStruct.type.name &&
      rgIdentificator.test(curStruct.type.name)
        ? curStruct.type.name
        : curStruct.name;
    const replaceHeadTypeParams: ReplaceParam[] = [
      { re: reStr.description, val: curStruct.descr },
      { re: reStr.descriptionAsComment, val: curStruct.descrAsComment() },
      { re: reStr.tagName, val: curStruct.name },
      { re: reStr.tagTypeName, val: validTypeName }
    ];
    const typeName = this.getPlIdentificator(validTypeName + suffixRec);
    const typeNameTbl = this.getPlIdentificator(validTypeName + suffixTbl);
    const vCurrentLevel = (curStruct && curStruct.level) || 0;
    let hashTbl;
    let lGenRead = genRead,
      lGenWrite = genWrite;
    // на нулевом уровне (корневые теги) определим надо ли генерировать
    // чтение/запись
    if (vCurrentLevel === 0) {
      if (
        this.rqMask.apply &&
        new RegExp(this.rqMask.mask.replace(/(\\)/g, "$1$1"), "gi").test(
          curStruct.name
        )
      ) {
        lGenWrite = this.rqMask.forWrite;
        lGenRead = this.rqMask.forRead;
      } else if (
        this.rsMask.apply &&
        new RegExp(this.rsMask.mask.replace(/(\\)/g, "$1$1"), "gi").test(
          curStruct.name
        )
      ) {
        lGenWrite = this.rsMask.forWrite;
        lGenRead = this.rsMask.forRead;
      } else {
        lGenWrite = true;
        lGenRead = true;
      }
    }
    const childGroup: GroupTypeNames[] = [];
    struct.childGroup.forEach((grItem) => {
      const gr: GroupTypeNames = {
        groupType: grItem.groupType,
        rows: []
      };
      grItem.rows.forEach((xsdElem) => {
        const elemHeaderStruct = xsdElem.headerRow.struct;
        const tdName =
          xsdElem && xsdElem.childGroup && xsdElem.childGroup.length
            ? this.getPlDefine({
                struct: xsdElem,
                genRead: lGenRead,
                genWrite: lGenWrite
              })
            : null;
        const itemTypeName =
          tdName ||
          (elemHeaderStruct &&
            elemHeaderStruct.type &&
            elemHeaderStruct.type.plTypeStr);
        if (tdName) {
          depencyTypes.push(tdName);
        }
        const el: StructTypeName | null =
          itemTypeName && elemHeaderStruct
            ? { struct: elemHeaderStruct, typeName: itemTypeName }
            : null;
        if (el) {
          gr.rows.push(el);
        }
      });
      childGroup.push(gr);
    });
    let orderIdx = 1; // вложенные типы сохраним в srcCode.levels[1]
    if (vCurrentLevel === 1) orderIdx = 0; // типы не вложенные в srcCode.levels[0], для того чтобы основные типы ввыести в самом низу
    const typeDefine = defineTypeSrc(childGroup, this.templates);
    if (typeDefine.length > 0 && vCurrentLevel) {
      // Зарегистрируем тип в srcCode.typesDefine (если такой тип уже есть вернем его имя)
      const { typeName: retTypeName, plType: td, isNew } = this.definePlType(
        typeName,
        // код определения типа, на основе шаблона struct_template
        getSrc(
          this.templates.struct,
          replaceHeadTypeParams.concat({ re: reStr.content, val: typeDefine })
        ),
        lGenRead,
        lGenWrite,
        curStruct.name,
        validTypeName,
        struct.typeHash || "",
        ""
      );
      retVal = retTypeName;
      // Если это новый тип,
      if (isNew && td) {
        const { readCode, writeCode, checkCode: checkNull } = readWriteCheckSrc(
          childGroup,
          this.templates,
          (name) => this.getTypeByName(name.toLowerCase()),
          (functCheckemptyName: string): void => {
            this.usedChecks.set(functCheckemptyName, 1);
          }
        );
        tdFillFuncDefine(
          td,
          replaceHeadTypeParams,
          lGenRead,
          lGenWrite,
          readCode,
          writeCode,
          checkNull,
          orderIdx,
          vCurrentLevel,
          false,
          this.templates
        );
        if (this.levels[orderIdx] === undefined) {
          this.levels[orderIdx] = [];
        }
        td.depencyTypes = depencyTypes;
        // добавим его в список типов для формирования исходников
        this.levels[orderIdx].push(td);
      } else if (td && orderIdx === 0) {
        // Если переиспользуется существующий тип, то добавим "пустышку" со ссылкой на используемый тип
        // const emptyTd: PlDefine = { name: typeName, fullname: name, refTd: td };
        const emptyTd: PlDefine = new PlDefine(
          typeName,
          curStruct.name,
          typeName,
          this.getReadFuncName(typeName),
          this.getWriteFuncName(typeName),
          "",
          "",
          "",
          "",
          "",
          "",
          td
        );
        emptyTd.depencyTypes = [td.name];
        emptyTd.typeDefine =
          "-- " +
          emptyTd.fullname +
          " идентичен " +
          td.name +
          "\n" +
          "type " +
          emptyTd.name +
          " is " +
          td.name +
          ";";
        emptyTd.functReadName = this.getReadFuncName(emptyTd.name);
        emptyTd.functWriteName = this.getWriteFuncName(emptyTd.name);
        emptyTd.functCheckemptyName = this.getCheckFuncName(emptyTd.name);
        tdFillFuncDefine(
          emptyTd,
          replaceHeadTypeParams,
          lGenRead,
          lGenWrite,
          "return " + td.functReadName + "(pMsgStruct, pTag);",
          td.functWriteName + "(pVal, pTag);",
          "return " + td.functCheckemptyName + "(pVal);",
          orderIdx,
          vCurrentLevel,
          true,
          this.templates
        );

        this.levels[orderIdx].push(emptyTd);
      }

      // Для табличного типа
      if (curStruct.maxOccurs > 1) {
        // получим код определения типа
        const typeDefine = getSrc(this.templates.table, replaceHeadTypeParams);
        hashTbl = String(stringHashCode("table" + struct.typeHash));
        // Зарегистрируем тип в srcCode.typesDefine (если такой тип уже есть вернем его имя)
        const {
          typeName: retTypeName,
          plType: tdTbl,
          isNew
        } = this.definePlType(
          typeNameTbl,
          typeDefine,
          false,
          false,
          curStruct.name,
          validTypeName,
          hashTbl,
          struct.typeHash || ""
        );
        retVal = retTypeName;
        // если тип новый
        if (isNew && tdTbl && td) {
          tdTbl.depencyTypes = [td.name.toLowerCase()];
          if (this.levels[orderIdx] === undefined) {
            this.levels[orderIdx] = [];
          }
          this.levels[orderIdx].push(tdTbl);
          tdTbl.writeFunct = "";
          tdTbl.writeFunctGlobalDef = "";
          tdTbl.readFunct = "";
          tdTbl.checkFunct = "";
          if (!!td.checkFunct) {
            tdTbl.checkFunct = getSrc(
              this.templates.procCheckemptyTbl,
              replaceHeadTypeParams.concat([
                {
                  re: reStr.functCheckemptyTblName,
                  val: tdTbl.functCheckemptyName
                },
                { re: reStr.functCheckemptyName, val: td.functCheckemptyName },
                { re: reStr.type, val: tdTbl.name }
              ])
            );
          }
          tdTbl.readFunctGlobalDef = "";
        }
      }
      // console.log('srcCode: '+JSON.stringify(srcCode));
    }
    return retVal;
  };

  // Получение типа по хэшу
  public getTypeByHash = (hash: string): PlDefine | undefined => {
    return this.namesByHash.get(hash)
      ? this.getTypeByName(this.namesByHash.get(hash))
      : undefined;
  };
  // Получение тип по имени
  public getTypeByName = (name?: string): PlDefine | undefined => {
    return name ? this.typesDefine.get(name.toLowerCase()) : undefined;
  };
  public namesByHash: Map<string, string> = new Map();
  public typesNamesUniq: Map<string, number> = new Map();
  public typesMap: Map<string, RowGroups[]> = new Map();
  public levels: Array<Array<PlDefine>> = [[], []];
  public sectionsGenGlobal: PlSection[] = [];
  public sectionsGenLocal: PlSection[] = [];
  public stLib: StLib;
  public typesDefine: Map<string, PlDefine> = new Map();
  public reservWordsReg: RegExp;
  public usedChecks: Map<string, number> = new Map();
  public getPlIdentificator: (
    name?: string,
    addStr?: string | number
  ) => string;
  public getReadFuncName: (name: string) => string;
  public getWriteFuncName: (name: string) => string;
  public getCheckFuncName: (name: string) => string;
  public constructor(
    rows: string,
    public templates: Templates = defaultTemplates,
    public dataTypesTemplates: DataTypeTemplates = defaultDataTypeTemplates,

    private rqMask: ApplyMaskState = defaultMask,
    private rsMask: ApplyMaskState = defaultMask,
    reservWordsStr?: string,
    genRead: boolean = true,
    genWrite: boolean = true
  ) {
    if (genRead) {
      this.sectionsGenGlobal.push("readFunctGlobalDef");
      this.sectionsGenLocal.push("readFunct");
    }
    if (genWrite) {
      this.sectionsGenGlobal.push("writeFunctGlobalDef");
      this.sectionsGenLocal.push("writeFunct");
    }
    const {
      funcReadString,
      funcWrite,
      funcReadNumber,
      funcReadBoolean,
      funcReadDate,
      funcWriteConvertDate
    } = getDataTypeTemplates(dataTypesTemplates);
    this.stLib = getStLib(
      funcReadString,
      funcWrite,
      funcReadNumber,
      funcWrite,
      funcReadNumber,
      funcWrite,
      funcReadBoolean,
      funcWrite,
      funcReadDate,
      funcWrite,
      funcWriteConvertDate
    );
    this.reservWordsReg = getReservWordReg(
      reservWordsStr || defaultReservWords
    );
    this.getPlIdentificator = getPlIdentificator(this.reservWordsReg);
    this.getReadFuncName = getReadFuncName(this.reservWordsReg);
    this.getWriteFuncName = getWriteFuncName(this.reservWordsReg);
    this.getCheckFuncName = getCheckFuncName(this.reservWordsReg);
    // удалим пробелы и лишние пустые строки, и создами массив строк
    const strs = strToXsdTextRows(rows.replace(/\n+/gm, "||").split("||"));
    const xsdHierarchy = new XsdTableHierarchy(
      { rowNum: 0, rowValue: "" },
      [
        {
          groupType: "other",
          rows: strs
        }
      ],
      0,
      this.stLib,
      this.reservWordsReg,
      new XsdNodePlPlus(0, "", "", "", 0, 0, this.stLib, this.reservWordsReg)
    );
    if (xsdHierarchy.childGroup && xsdHierarchy.childGroup.length) {
      const typesStore = { typesMap: new Map<string, RowGroups[]>() };
      xsdHierarchy.getTypeString(typesStore);
      this.typesMap = typesStore.typesMap;
      this.getPlDefine({ struct: xsdHierarchy, genRead: true, genWrite: true });
    }
  }
  public global = (): string => {
    const { headPlplusGlobal } = this.templates;
    const retVal = ["/*\n * Генератор pl/plus " + versionScript + "\n */"]
      .concat(headPlplusGlobal)
      .concat(
        this.levelsSrcTxt(
          [this.levels[1]],
          ["typeDefine"],
          "------------ Вложенные комплексные типы -----------"
        )
      )
      .concat(
        this.levelsSrcTxt(
          [this.levels[0]],
          ["typeDefine"],
          "------------ Типы основных запросов -----------"
        )
      )
      .concat(
        this.levelsSrcTxt(
          [this.levels[1]],
          this.sectionsGenGlobal,
          "------------ Операции чтения/записи комплексных типов -----------"
        )
      )
      .concat(
        this.levelsSrcTxt(
          [this.levels[0]],
          this.sectionsGenGlobal,
          "------------ Операции чтения/записи основных типов -----------"
        )
      );

    return retVal.filter((item) => item).join("\n\n");
  };
  public local = (): string => {
    const { headPlplusLocal } = this.templates;
    const retVal = [headPlplusLocal]
      .concat(
        this.levelsSrcTxt(
          [this.levels[1]],
          this.sectionsGenLocal,
          "------------ Операции чтения/записи комплексных типов -----------"
        )
      )
      .concat(
        this.levelsSrcTxt(
          [this.levels[0]],
          this.sectionsGenLocal,
          "------------ Операции чтения/записи основных типов -----------"
        )
      );

    return retVal.filter((item) => item).join("\n\n");
  };
  public toString = (): string => {
    return (
      "--------- Глобальная секция --------------\n" +
      this.global() +
      "\n\n--------- Локальная секция --------------\n" +
      this.local()
    );
  };
}
