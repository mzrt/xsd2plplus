const readPlElement = () => "readPl\n";
const writePlElement = () => "writePl\n";
const checkPlElement = () => "checkPl";
const mocked = (): string => {
  console.log("mock module");
  return "mocked";
};
export {};
module.exports = {
  readPlElement,
  writePlElement,
  checkPlElement,
  mocked
};
