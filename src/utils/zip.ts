type GetDataCallback = (
  blobWriter: unknown,
  uncompressDataCallback: (text: string) => void,
  uncompressProgressCallback: (current: number, total: number) => void
) => void;
type GetEntriesCallback = (
  arg0: (entries: { getData: GetDataCallback }[]) => void
) => void;
type CreateWriterCallback = (zipWriter: {
  add: (arg0: string, arg1: unknown, arg2: () => void) => void;
  close: (arg0: (zippedBlob: unknown) => void) => void;
}) => void;
type ZipReader = {
  getEntries: GetEntriesCallback;
  close: () => void;
};
type CreateReaderCallback = (zipReader: ZipReader) => void;
declare let zip: {
  BlobWriter: any;
  BlobReader: any;
  createWriter: (
    writer: unknown,
    fn: CreateWriterCallback,
    onError: (message: string) => void
  ) => void;
  createReader: (
    writer: unknown,
    fn: CreateReaderCallback,
    onError: (message: string) => void
  ) => void;
};

export function onerror(message: string): void {
  console.error(message);
}
const { BlobWriter } = zip;
export function zipBlob(
  filename: string,
  blob: Blob,
  callback: (zippedBlob: unknown) => void
): void {
  // use a zip.BlobWriter object to write zipped data into a Blob object
  zip.createWriter(
    new BlobWriter("application/zip"),
    function (zipWriter: {
      add: (arg0: string, arg1: unknown, arg2: () => void) => void;
      close: (arg0: (zippedBlob: unknown) => void) => void;
    }) {
      // use a BlobReader object to read the data stored into blob variable
      zipWriter.add(filename, new zip.BlobReader(blob), function () {
        // close the writer and calls callback function
        zipWriter.close(callback);
      });
    },
    onerror
  );
}
export function unzipBlob(
  blob: unknown,
  callback: (data: string, idx?: number) => void
): void {
  // use a zip.BlobReader object to read zipped data stored into blob variable
  zip.createReader(
    new zip.BlobReader(blob),
    function (zipReader: ZipReader) {
      // get entries from the zip file
      zipReader.getEntries(function (entries) {
        // get data from the first file
        entries.forEach(
          (
            item: {
              getData: GetDataCallback;
            },
            idxEntry: number
          ) => {
            item.getData(
              new zip.BlobWriter("text/plain"),
              function (text: string) {
                // close the reader and calls callback function with uncompressed data as parameter
                callback(text, idxEntry);
              },
              function (current: number, total: number): void {
                // onprogress callback
                console.log(current, total);
              }
            );
          }
        );
        // zipReader.close();
      });
    },
    onerror
  );
}
/*
// create the blobLoremTxt object storing the data to compress
const blobLoremTxt = new Blob(
  ["Lorem ipsum dolor sit amet, consectetuer adipiscing elit..."],
  {
    type: "text/plain"
  }
);
const reader = new FileReader();
// This fires after the blob has been read/loaded.
reader.addEventListener("loadend", (e: any) => {
  const text = e && e.srcElement && e.srcElement.result;
  console.log(text);
});

// creates a zip storing the file "lorem.txt" with blob as data
// the zip will be stored into a Blob object (zippedBlob)
zipBlob("lorem.txt", blobLoremTxt, function(zippedBlob) {
  // unzip the first file from zipped data stored in zippedBlob
  unzipBlob(zippedBlob, function(unzippedBlob: any) {
    // logs the uncompressed Blob
    console.log(
      "zip lorem.txt and unzip test unzippedBlob",
      JSON.stringify(unzippedBlob),
      unzippedBlob
    );

    // Start reading the blob as text.
    reader.readAsText(unzippedBlob);
  });
});
*/
