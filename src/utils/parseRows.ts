import {
  defaultDataTypeTemplates,
  getReservWordReg,
  defaultReservWords
} from "./TemplatesDataType";
const {
  funcReadString,
  funcWrite,
  funcReadNumber,
  funcReadBoolean,
  funcReadDate,
  funcWriteConvertDate
} = defaultDataTypeTemplates;
const hashCode = (str: string): number => {
  let hash = 0,
    i,
    chr,
    len;
  if (str.length === 0) return hash;
  for (i = 0, len = str.length; i < len; i++) {
    chr = str.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; //convert to 32bit integer
  }
  return hash;
};
export const suffixTbl = "_tbl";
export const suffixRec = "_rec";
export const plOptions = {
  suffixTbl,
  suffixRec,
  sufx_tr: "(" + suffixTbl + "|" + suffixRec + ")",
  maxLengthDefine: 30,
  pl_ret_var: "retVal"
};

// Функция возвращает идентификатор удовлетворяющий требованиям pl/plus
// если имя требуется укоротить, то стараемся сохранить окончания _rec, _tbl, Rq, Rs, если таковые имеются
// addStr добавляется в наименование перед (Rq|Rs)_rec, (Rq|Rs)_tbl
export const getPlIdentificator = (reservWordsReg: RegExp) => (
  name: string = "",
  addStr?: string | number
): string => {
  const { maxLengthDefine, suffixRec, sufx_tr } = plOptions;
  let ret_val = "";
  if (name) {
    let addLength = 0;
    const l_addStr = !addStr ? "" : addStr;
    if (new RegExp("^.*" + suffixTbl + "$", "i").test(name)) {
      addLength = addLength + suffixTbl.length;
    }
    if (new RegExp("^.*" + suffixRec + "$", "i").test(name)) {
      addLength = addLength + suffixRec.length;
    }
    if (
      new RegExp("^.*(Rq|Rs)(" + suffixRec + "|" + suffixTbl + ")*$", "i").test(
        name
      )
    ) {
      addLength = addLength + 2;
    }
    const maxLreg =
      "{0," + (maxLengthDefine - addLength - ("" + l_addStr).length) + "}";
    const regexp = new RegExp(
      "^(." +
        maxLreg +
        ").*((rq|rs)" +
        sufx_tr +
        "*)$|^(." +
        maxLreg +
        ").*" +
        sufx_tr +
        "$|^(." +
        maxLreg +
        ").*$",
      "i"
    );
    ret_val = name.replace(regexp, "$1$5$3$7" + l_addStr + "$4$6");
    if (reservWordsReg.test(ret_val)) ret_val = '"' + ret_val + '"';
  }
  return ret_val;
};
const getFuncName = (
  prefix: string
): ((reservWordsReg: RegExp) => (name: string) => string) => (
  reservWordsReg: RegExp
) => (name: string): string => {
  const { sufx_tr } = plOptions;
  return getPlIdentificator(reservWordsReg)(
    prefix + name.replace(new RegExp(sufx_tr + "$", "i"), "")
  );
};
export const getReadFuncName = getFuncName("r_");
export const getWriteFuncName = getFuncName("w_");
export const getCheckFuncName = getFuncName("c_");
export class PlSimpleType {
  public name: string;
  public re: RegExp;
  public plName: string;
  public plRead: string;
  public plWrite: string;
  public plWriteConv: string;
  // Конструктор структуры простого типа
  public constructor(
    name: string,
    re: RegExp,
    plName: string,
    plRead: string,
    plWrite: string,
    plWriteConv: string
  ) {
    this.name = name;
    this.re = re;
    this.plName = plName;
    this.plRead = plRead;
    this.plWrite = plWrite;
    this.plWriteConv = plWriteConv;
  }
}
export type StLib = Map<string, PlSimpleType>;

// Определяет по строковому описанию тип из набора типов st_lib
const getPlSimpleType = (
  type: string,
  stLib: StLib
): PlSimpleType | undefined => {
  return Array.from(stLib.values()).find(
    (val): boolean => val && type.search(val.re) > -1
  );
};

export class NodeType {
  public name?: string;
  public minLength: number;
  public pattern?: string;
  public maxLength?: number;
  public plSimpleType?: PlSimpleType;
  public plTypeStr?: string;
  public constructor(typeStr: string, typesMap: StLib) {
    this.minLength = 0;
    if (typeStr) {
      // Тип
      this.name = typeStr
        .replace(new RegExp("xsd\\:\\s*", "i"), "xs:")
        .replace(new RegExp("^(\\S+)($|\\s.*)|.*"), "$1")
        .replace(new RegExp("Boolean"), "boolean");
      // Минимальная длина
      try {
        this.minLength = Number(
          typeStr.replace(new RegExp('^.*minLength="?(\\d+)"?.*|.*'), "$1")
        );
      } catch (error) {
        this.minLength = 0;
      }
      // Шаблон
      this.pattern = typeStr.replace(
        new RegExp('(^.*pattern="?([^"]*)"?].*)|.*', "ig"),
        "$2"
      );
      // Максимальная длина
      let tempVal = 0;
      try {
        tempVal = Number(
          typeStr.replace(new RegExp('^.*maxLength="?(\\d+)"?.*|.*'), "$1")
        );
      } catch (error) {
        tempVal = 0;
      }
      if (this.pattern.length > 0 && (!tempVal || isNaN(tempVal))) {
        // Если максимальная длина не задана явно, и при этом есть шаблон, то пробуем определить максимальную длину по шаблону
        try {
          tempVal = Number(
            // eslint-disable-next-line no-eval
            eval(
              this.pattern
                .replace(/[?]/g, "")
                .replace(/\\.|([^{])\d([^}])/g, "$1s$2")
                .replace(/\\.|([^{])\d([^}])/g, "$1s$2")
                .replace(/\[[^[\]]+\]/g, "+1")
                .replace(/\(([^|]+)(\|[^)]+)*\)/g, "+($1)")
                .replace(/\{(\d+,\s*)?(\d+)\}/g, "{$2}")
                .replace(/([^()+{}\d]+)/g, '+"$1".length')
                .replace(/\{(\d+)\}/g, "*$1")
                .replace(/\+*\(/g, "+(")
            )
          );
        } catch (err) {}
      }
      this.maxLength = isNaN(tempVal) ? 0 : tempVal;
      // получим структуру простого типа
      this.plSimpleType = getPlSimpleType(this.name, typesMap);
      // если удалось определить простой тип, то заполним pl/plus тип
      if (this.plSimpleType && this.plSimpleType.name) {
        if (this.plSimpleType.name === "string") {
          this.plTypeStr =
            this.plSimpleType.plName +
            "(" +
            (this.maxLength === undefined || this.maxLength === 0
              ? 32767
              : this.maxLength) +
            ")";
        } else {
          this.plTypeStr = this.plSimpleType && this.plSimpleType.plName;
        }
      }
    }
  }
}
// Конструктор структуры для хранения распарсенной строки
export class XsdNodePlPlus {
  public level: number;
  public name: string;
  public plName: string;
  public type: NodeType;
  public descr: string;
  public minOccurs: number;
  public maxOccurs: number;
  public constructor(
    level: number,
    name: string,
    type: string,
    descr: string,
    minOccurs: number,
    maxOccurs: number,
    typesMap: StLib,
    reservWordsReg: RegExp
  ) {
    this.level = level;
    this.name = name;
    this.plName = getPlIdentificator(reservWordsReg)(name);
    this.type = new NodeType(type, typesMap);
    this.descr = descr;
    this.minOccurs = minOccurs;
    this.maxOccurs = maxOccurs;
  }
  public descrAsComment = (): string => {
    return this.descr.replace(new RegExp("(^)(.+)$", "gm"), "$1-- $2");
  };
}
export interface XsdTextRow {
  rowNum: number;
  rowValue: string;
}
export interface XsdTextRowStruct extends XsdTextRow {
  struct?: XsdNodePlPlus;
}
export interface RowsHierarchy {
  headerRow: XsdTextRowStruct;
  childGroup: RowGroups[];
  typeHash?: string;
  getTypeString: (typeStore: { typesMap: Map<string, RowGroups[]> }) => string;
}
export type GroupType = "choice" | "other";
export interface RowGroupsBase {
  groupType: GroupType;
}
export interface RowGroups extends RowGroupsBase {
  rows: RowsHierarchy[];
}
export interface RowGroupsRaw extends RowGroupsBase {
  rows: XsdTextRow[];
}
export const embededTypesOpenCloseDefault: string = "[";
export const embededTypesOpenDefault: string = "┌";
export const embededTypesInDefault: string = "│";
export const embededTypesCloseDefault: string = "└";
export const levelMarkerDefault = "\\t";
export const levelMarkerDefaultNoEsc = "\t";
export const getRegs = (
  levelMarker = levelMarkerDefault,
  embededTypesOpenClose = embededTypesOpenCloseDefault,
  embededTypesOpen = embededTypesOpenDefault,
  embededTypesIn = embededTypesInDefault,
  embededTypesClose = embededTypesCloseDefault
): {
  regChildRow: RegExp;
  regEmbededTypesOpenClose: RegExp;
  regEmbededTypesOpen: RegExp;
  regEmbededTypesOpenChoice: RegExp;
  regEmbededTypesIn: RegExp;
  regEmbededTypesClose: RegExp;
  regLevelClear: RegExp;
  levelClear: (str: string) => string;
} => {
  const expBeg = "^[ " + String.fromCharCode(160) + "]*";
  const expEnd = ".*\\w+.*";
  const regChildRow = new RegExp(
    expBeg +
      "(" +
      levelMarker +
      "|" +
      "\\" +
      embededTypesOpenClose +
      levelMarker +
      "|" +
      "\\" +
      embededTypesOpen +
      levelMarker +
      "|" +
      "\\" +
      embededTypesIn +
      levelMarker +
      "|" +
      "\\" +
      embededTypesClose +
      levelMarker +
      ")" +
      expEnd
  );
  const regEmbededTypesOpenClose = new RegExp(
    expBeg + "\\" + embededTypesOpenClose + levelMarker + expEnd
  );
  const regEmbededTypesOpen = new RegExp(
    expBeg + "\\" + embededTypesOpen + levelMarker + expEnd
  );
  const regEmbededTypesOpenChoice = new RegExp(
    expBeg + "\\" + embededTypesOpen + levelMarker + "(\\w+)[┌]choice"
  );
  const regEmbededTypesIn = new RegExp(
    expBeg + "\\" + embededTypesIn + levelMarker + expEnd
  );
  const regEmbededTypesClose = new RegExp(
    expBeg + "\\" + embededTypesClose + levelMarker + expEnd
  );
  const regLevelClear = new RegExp(
    expBeg +
      "(" +
      levelMarker +
      "|" +
      "\\" +
      embededTypesOpenClose +
      levelMarker +
      "|" +
      "\\" +
      embededTypesOpen +
      levelMarker +
      "|" +
      "\\" +
      embededTypesIn +
      levelMarker +
      "|" +
      "\\" +
      embededTypesClose +
      levelMarker +
      ")"
  );
  const levelClear = (str: string): string =>
    str
      .replace(regLevelClear, "")
      .replace(/(^\w+)┌(choice|sequence)(\[.+?\])*(.*)|(.*)$/gm, "$1$4$5");
  return {
    regChildRow,
    regEmbededTypesOpenClose,
    regEmbededTypesOpen,
    regEmbededTypesOpenChoice,
    regEmbededTypesIn,
    regEmbededTypesClose,
    regLevelClear,
    levelClear
  };
};
// валидатор строк
export const validateRows = (
  rows: XsdTextRow[],
  levelMarker: string = levelMarkerDefault,
  embededTypesOpenClose = embededTypesOpenCloseDefault,
  embededTypesOpen = embededTypesOpenDefault,
  embededTypesIn = embededTypesInDefault,
  embededTypesClose = embededTypesCloseDefault
): string[] => {
  const { regChildRow } = getRegs(
    levelMarker,
    embededTypesOpenClose,
    embededTypesOpen,
    embededTypesIn,
    embededTypesClose
  );
  if (rows && rows.length && rows[0].rowValue.search(regChildRow) > -1) {
    return [
      "Ошибка в текстовом представлении xsd-схемы в строке " +
        rows[0].rowNum +
        ". Некорректный отступ!"
    ];
  }
  return [];
};

// Получение значения по индексу,
// с удалением символов перевода строки, пробела в начале и конце значения
export const getField = (arr: string[], idx: number): string => {
  let ret_val = "";
  if (arr.length > idx) {
    ret_val = arr[idx].replace(/^[\n\s\r]+|[\n\r\s]+$/gi, "");
  }
  return ret_val;
};
// Парсинг строки и сохранение в структуру
export const getRow = (
  str: string,
  level: number,
  typesMap: StLib,
  reservWordsReg: RegExp
): XsdNodePlPlus | undefined => {
  const cols = str.split("\t");
  let ret_val;
  let idx, name;
  let nameIdx = 0;
  let nameIdxDefine = false;
  let col_min_max_occurs;
  const clearName = /\s*[│]\s*|@|:/gi;
  for (idx = 0; idx < cols.length; ++idx) {
    name = getField(cols, idx);
    name = name.replace(clearName, "");
    if (!nameIdxDefine) {
      if (name.length > 0) {
        nameIdxDefine = true;
      } else nameIdx++;
    } else {
      if (/^\[.+\]$/gi.test(name)) {
        col_min_max_occurs = idx;
        break;
      }
    }
  }
  if (col_min_max_occurs !== undefined && col_min_max_occurs >= 3) {
    name = getField(cols, nameIdx);
    name = name.replace(clearName, "");
    // if (isStartChoice)name = name.replace(/(.+)[\┌]choice.*/gi, '$1');
    const type = getField(cols, col_min_max_occurs - 2);
    // if (re_lib.choice.test(type))name='choice';
    const descr = getField(cols, col_min_max_occurs - 1);
    const occurs = getField(cols, col_min_max_occurs);
    const minOccurs = occurs.replace(new RegExp("^\\[(\\d+)\\D*.*"), "$1");
    const tempVal = occurs.replace(new RegExp(".*\\D*(\\d+|N)\\]$"), "$1");
    const maxOccurs = tempVal === "N" ? Infinity : Number(tempVal);
    ret_val = new XsdNodePlPlus(
      level,
      name,
      type,
      descr,
      Number(minOccurs),
      maxOccurs,
      typesMap,
      reservWordsReg
    );
  }
  return ret_val;
};
// Класс для иерархического упорядочивания строк
export class XsdTableHierarchy implements RowsHierarchy {
  public childGroup: RowGroups[];
  public headerRow: XsdTextRowStruct;
  public typeHash?: string = "";

  public constructor(
    headerRow: XsdTextRow,
    rowGroups: RowGroupsRaw[],
    level: number,
    typesMap: StLib,
    reservWordsReg: RegExp = getReservWordReg(defaultReservWords),
    headStruct?: XsdNodePlPlus,
    levelMarker: string = levelMarkerDefault,
    embededTypesOpenClose = embededTypesOpenCloseDefault,
    embededTypesOpen = embededTypesOpenDefault,
    embededTypesIn = embededTypesInDefault,
    embededTypesClose = embededTypesCloseDefault
  ) {
    const struct =
      headStruct || getRow(headerRow.rowValue, level, typesMap, reservWordsReg);
    this.headerRow = { ...headerRow, struct };
    this.childGroup = [];
    const {
      regChildRow,
      regEmbededTypesOpen,
      regEmbededTypesOpenChoice,
      regEmbededTypesClose,
      levelClear
    } = getRegs(
      levelMarker,
      embededTypesOpenClose,
      embededTypesOpen,
      embededTypesIn,
      embededTypesClose
    );
    this.childGroup = rowGroups.map(
      ({ groupType, rows }): RowGroups => {
        const retVal: XsdTableHierarchy[] = [];
        if (rows && rows.length && !validateRows(rows, levelMarker).length) {
          // Ищем строки текущего уровня
          // Первая строка всегда будет текущего уровня
          let currentChildHeaderRow = rows[0];
          for (let i = 1; i < rows.length; i++) {
            // Ищем дочерние строки для строки текущего уровня
            let j = i;
            let startGroup = j;
            const groups: { start: number; end: number }[] = [];
            for (
              ;
              j < rows.length && rows[j].rowValue.search(regChildRow) > -1;
              j++
            ) {
              if (rows[j].rowValue.search(regEmbededTypesOpen) > -1) {
                if (startGroup < j) {
                  groups.push({
                    start: startGroup,
                    end: j - 1
                  });
                  startGroup = j;
                }
              }
              if (rows[j].rowValue.search(regEmbededTypesClose) > -1) {
                if (startGroup < j) {
                  // Найдем последний вложенный элемент
                  for (
                    ;
                    j + 1 < rows.length &&
                    levelClear(rows[j + 1].rowValue) &&
                    levelClear(rows[j + 1].rowValue).search(regChildRow) > -1;
                    j++
                  ) {}
                  groups.push({
                    start: startGroup,
                    end: j
                  });
                  startGroup = j + 1;
                }
              }
            }
            if (startGroup < j) {
              groups.push({
                start: startGroup,
                end: j - 1
              });
            }
            retVal.push(
              new XsdTableHierarchy(
                currentChildHeaderRow,
                groups.map(
                  ({ start, end }): RowGroupsRaw => ({
                    groupType:
                      rows[start].rowValue.search(regEmbededTypesOpenChoice) >
                      -1
                        ? "choice"
                        : "other",
                    rows: rows.slice(start, end + 1).map(
                      ({ rowNum, rowValue }): XsdTextRow => ({
                        rowNum,
                        rowValue: levelClear(rowValue)
                      })
                    )
                  })
                ),
                level + 1,
                typesMap,
                reservWordsReg,
                undefined,
                levelMarker,
                embededTypesOpenClose,
                embededTypesOpen,
                embededTypesIn,
                embededTypesClose
              )
            );
            currentChildHeaderRow = rows[j];
            i = j;
          }
          if (currentChildHeaderRow) {
            retVal.push(
              new XsdTableHierarchy(
                currentChildHeaderRow,
                [],
                level + 1,
                typesMap,
                reservWordsReg,
                undefined,
                levelMarker,
                embededTypesOpenClose,
                embededTypesOpen,
                embededTypesIn,
                embededTypesClose
              )
            );
          }
        }
        return { groupType, rows: retVal };
      }
    );
  }
  public getTypeString = (
    typeStore: {
      typesMap: Map<string, RowGroups[]>;
    },
    force: boolean = false
  ): string => {
    let retVal: string = "";
    if (
      this.childGroup.length &&
      this.childGroup.reduce((result, gr) => result + (gr.rows.length || 0), 0)
    ) {
      retVal = this.typeHash || "";
      if (!retVal || force || !typeStore.typesMap.get(retVal)) {
        retVal = String(
          hashCode(
            this.childGroup
              .map((gr) => {
                return (
                  gr.groupType +
                  "(" +
                  gr.rows.reduce((resultChilds, item) => {
                    const itemTypeStr = item.getTypeString(typeStore);
                    return (
                      resultChilds +
                      (itemTypeStr && item.headerRow.struct
                        ? "|" +
                          item.headerRow.struct.name +
                          ":" +
                          itemTypeStr +
                          ":" +
                          item.headerRow.struct.minOccurs +
                          ":" +
                          item.headerRow.struct.maxOccurs
                        : "")
                    );
                  }, "") +
                  ")"
                );
              })
              .join("|")
          )
        );
        this.typeHash = retVal;
      }
      if (!typeStore.typesMap.get(retVal)) {
        typeStore.typesMap.set(retVal, this.childGroup);
      }
    }
    if (
      !retVal &&
      this.headerRow.struct &&
      this.headerRow.struct.type &&
      this.headerRow.struct.type.plTypeStr
    ) {
      retVal =
        this.headerRow.struct &&
        this.headerRow.struct.type &&
        this.headerRow.struct.type.plTypeStr;
    } else if (this.headerRow.struct && this.headerRow.struct.type) {
      this.headerRow.struct.type.plSimpleType = undefined;
      this.headerRow.struct.type.plTypeStr = undefined;
    }
    return retVal;
  };
}
export const cleanStrRows = (strs: string[]): string[] => {
  return [];
};
export const strToXsdTextRows = (strs: string[]): XsdTextRow[] => {
  return strs.map(
    (str, index): XsdTextRow => ({ rowNum: index + 1, rowValue: str })
  );
};
const reStart = "(^|[\\s\\W])";
const reFinish = "([\\s\\W]|$)";
export const reLib = {
  // Типы
  varhcar: new RegExp(reStart + "string" + reFinish, "i"),
  number: new RegExp(reStart + "decimal|float|long|double" + reFinish, "i"),
  plsInteger: new RegExp(reStart + "integer|byte|int" + reFinish, "i"),
  boolean: new RegExp(reStart + "boolean" + reFinish, "i"),
  choice: new RegExp(reStart + "choice" + reFinish, "i"),
  date: new RegExp(reStart + "date" + reFinish, "i")
};
export const getStLib = (
  pFuncReadString: string = funcReadString,
  pFuncWriteString: string = funcWrite,
  pFuncReadNumber: string = funcReadNumber,
  pFuncWriteNumber: string = funcWrite,
  pFuncReadInteger: string = funcReadNumber,
  pFuncWriteInteger: string = funcWrite,
  pFuncReadBoolean: string = funcReadBoolean,
  pFuncWriteBoolean: string = funcWrite,
  pFuncReadDate: string = funcReadDate,
  pFuncWriteDate: string = funcWrite,
  pFuncWriteConvertDate: string = funcWriteConvertDate
): StLib => {
  return new Map<string, PlSimpleType>([
    [
      "string",
      new PlSimpleType(
        "string",
        reLib.varhcar,
        "varchar2",
        pFuncReadString,
        pFuncWriteString,
        ""
      )
    ],
    [
      "number",
      new PlSimpleType(
        "number",
        reLib.number,
        "number",
        pFuncReadNumber,
        pFuncWriteNumber,
        ""
      )
    ],
    [
      "integer",
      new PlSimpleType(
        "integer",
        reLib.plsInteger,
        "pls_integer",
        pFuncReadInteger,
        pFuncWriteInteger,
        ""
      )
    ],
    [
      "boolean",
      new PlSimpleType(
        "boolean",
        reLib.boolean,
        "boolean",
        pFuncReadBoolean,
        pFuncWriteBoolean,
        ""
      )
    ],
    [
      "date",
      new PlSimpleType(
        "date",
        reLib.date,
        "date",
        pFuncReadDate,
        pFuncWriteDate,
        pFuncWriteConvertDate
      )
    ],
    [
      "choice",
      new PlSimpleType("choice", reLib.choice, "varchar2(32)", "", "", "")
    ]
  ]);
};
