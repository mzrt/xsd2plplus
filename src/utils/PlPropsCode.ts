import { XsdNodePlPlus } from "./parseRows";
import {
  PlDefine,
  ReplaceParam,
  reStr,
  rgIdentificator,
  getSrc
} from "./PlPlusGenerator";
import { Templates } from "./templates";

// Генератор кода для чтения тега
export const readPlElement = (
  curStruct: XsdNodePlPlus,
  typeStruct: PlDefine | undefined,
  isChoice: boolean,
  { elemReadSimple, tableRead, elemRead, read, elemReadChoice }: Templates
): string => {
  let retVal = "";
  let replaceParams: ReplaceParam[] = [
    { re: reStr.tagName, val: curStruct.name },
    {
      re: reStr.tagTypeName,
      val:
        curStruct.type &&
        curStruct.type.name &&
        rgIdentificator.test(curStruct.type.name)
          ? curStruct.type.name
          : curStruct.name
    },
    { re: reStr.plName, val: curStruct.plName }
  ];

  if (curStruct && curStruct.type && curStruct.type.plSimpleType) {
    // Простой тип
    replaceParams.push({
      re: reStr.readSimpleTypeName,
      val: curStruct.type.plSimpleType.plRead
    });
    retVal = getSrc(
      elemReadSimple,
      replaceParams.concat({
        re: reStr.readSimpleTypeName,
        val: curStruct.type.plSimpleType.plRead
      })
    );
  } else if (typeStruct) {
    // Сложный тип
    replaceParams.push({
      re: reStr.functReadName,
      val: typeStruct.functReadName
    });
    if (curStruct.maxOccurs > 1) {
      retVal = getSrc(tableRead, replaceParams);
    } else {
      retVal = getSrc(elemRead, replaceParams);
    }
    replaceParams = [
      { re: reStr.tagName, val: curStruct.name },
      {
        re: reStr.tagTypeName,
        val:
          curStruct.type &&
          curStruct.type.name &&
          rgIdentificator.test(curStruct.type.name)
            ? curStruct.type.name
            : curStruct.name
      },
      { re: reStr.content, val: retVal }
    ];
    retVal = getSrc(read, replaceParams);
  }
  // Выбор
  if (retVal.length > 0 && isChoice) {
    retVal = retVal + getSrc(elemReadChoice, replaceParams);
  }
  return retVal;
};
// Генератор кода для записи тега
export function writePlElement(
  curStruct: XsdNodePlPlus,
  typeStruct: PlDefine | undefined,
  isChoice: boolean,
  {
    elemWriteSimple,
    tableWrite,
    elemWrite,
    write,
    writeCheck,
    elemWriteChoice
  }: Templates,
  onCountChecks: (checkFuncName: string) => void
): string {
  let retVal = "";
  let replaceParams = [];
  let template = "";

  replaceParams = [
    { re: reStr.tagName, val: curStruct.name },
    {
      re: reStr.tagTypeName,
      val:
        curStruct.type &&
        curStruct.type.name &&
        rgIdentificator.test(curStruct.type.name)
          ? curStruct.type.name
          : curStruct.name
    },
    { re: reStr.plName, val: curStruct.plName }
  ];

  if (curStruct.type && curStruct.type.plSimpleType) {
    // простой тип
    template = elemWriteSimple;
    replaceParams.push({
      re: reStr.writeSimpleTypeName,
      val: curStruct.type.plSimpleType.plWrite
    });
    if (
      curStruct.type.plSimpleType.plName === "date" &&
      curStruct.type.plSimpleType.plWriteConv.length > 0
    )
      template = template.replace(
        new RegExp("(\\S+\\." + reStr.plName.rstr + ")", "gim"),
        curStruct.type.plSimpleType.plWriteConv + "($1)"
      );
    retVal = getSrc(template, replaceParams);
  } else if (typeStruct) {
    // сложный тип
    replaceParams.push({
      re: reStr.functWriteName,
      val: typeStruct.functWriteName
    });
    if (curStruct.maxOccurs > 1) {
      retVal = getSrc(tableWrite, replaceParams);
    } else {
      retVal = getSrc(elemWrite, replaceParams);
    }
    retVal = getSrc(write, [
      { re: reStr.tagName, val: curStruct.name },
      {
        re: reStr.tagTypeName,
        val:
          curStruct.type &&
          curStruct.type.name &&
          rgIdentificator.test(curStruct.type.name)
            ? curStruct.type.name
            : curStruct.name
      },
      { re: reStr.content, val: retVal }
    ]);
    // Проверка на пустое значение для необязательных реквизитов
    if (0 + (curStruct.minOccurs || 0) === 0) {
      retVal = getSrc(writeCheck, [
        { re: reStr.content, val: retVal },
        {
          re: reStr.contentCheck,
          val:
            typeStruct.functCheckemptyName + "(p_val." + curStruct.plName + ")"
        }
      ]);
      onCountChecks(typeStruct.functCheckemptyName);
    }
  }
  // Обернем запись в choice если это требуется
  if (retVal.length > 0 && isChoice /*&& !(curStruct.maxOccurs>1)*/) {
    replaceParams.push({ re: reStr.content, val: retVal });
    retVal = getSrc(elemWriteChoice, replaceParams);
  }
  return retVal;
}
// Генератор кода для проверки на пустое значение тега
export function checkPlElement(
  curStruct: XsdNodePlPlus,
  typeStruct: PlDefine | undefined,
  isChoice: boolean
): string {
  let retVal = "";

  const membName = "p_val." + curStruct.plName;
  const choicePart = isChoice
    ? "p_val.choice='" + curStruct.plName + "' and "
    : "";
  if (curStruct.type && curStruct.type.plTypeStr) {
    // простой тип
    retVal =
      "if " + choicePart + membName + " is not null then return true; end if;";
  } else if (typeStruct) {
    // сложный тип
    retVal =
      "if " +
      choicePart +
      typeStruct.functCheckemptyName +
      "(" +
      membName +
      ") then return true; end if;";
  }
  return retVal;
}
export const mocked = (): string => {
  console.log("original");
  return "not mocked";
};
