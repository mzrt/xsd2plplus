import { globaldefaults } from "../constantsDefaultGeneratorSettingsWrapped";

const {
  elemReadSimple,
  tableRead,
  elemRead,
  read,
  elemReadChoice,
  elemWriteSimple,
  tableWrite,
  elemWrite,
  write,
  writeCheck,
  elemWriteChoice,
  procRootRead,
  procRead,
  procRootWrite,
  procWrite,
  procCheckempty,
  headPlplusGlobal,
  headPlplusLocal,
  struct,
  structItem,
  table,
  procCheckemptyTbl
} = globaldefaults;
export const tStr = (template: string[]): string => template.join("\n");

// возвращает заголовок функции/процедуры для использования в секции глобальных объявлений
export const getPlFuncHead = (template: string): string => {
  return (
    template
      .split("\n")
      .join("~~")
      .replace(
        /^(.*(function|procedure).+)(\))(\s|\t|~~)*(return(\s|\t|~~)+.+)*(\s|\t|~~)+is(\s|\t|~~).*|.*/i,
        "$1$3$5"
      )
      .split("~~")
      .join("\n") + ";"
  );
};
export const shortRead = (readFunc: string): string => {
  return readFunc
    .split("\n")
    .join("~~")
    .replace(
      /^(.*)(\s|\t|~~)+pnIdx.*(.*begin~~).*(\s|\t|~~)+(\[content\]~~).*(\s|\t|~~)*(end;--.*$)|.*/i,
      "$1$3$4$5$6$7"
    )
    .split("~~")
    .join("\n");
};
export const shortWrite = (writeFunc: string): string => {
  return writeFunc
    .split("\n")
    .join("~~")
    .replace(
      /^(.*)(\s|\t|~~)+pnIdx.*(.*begin~~).*(\s|\t|~~)+(\[content\]~~).*(\s|\t|~~)*(end;--.*$)|.*/i,
      "$1$3$4$5$6$7"
    )
    .split("~~")
    .join("\n");
};
export const shortCheck = (checkFunc: string): string => {
  return checkFunc
    .split("\n")
    .join("~~")
    .replace(
      /^(.*begin~~).*(\s|\t|~~)+(\[content\]~~).*(end;--.*$)|(.*)/i,
      "$1$2$3$4$5"
    )
    .split("~~")
    .join("\n");
};

export interface Templates {
  elemReadSimple: string;
  tableRead: string;
  elemRead: string;
  read: string;
  elemReadChoice: string;
  elemWriteSimple: string;
  tableWrite: string;
  elemWrite: string;
  write: string;
  writeCheck: string;
  elemWriteChoice: string;

  procRootRead: string;
  procRootReadS: string;
  procRootReadHead: string;
  procRead: string;
  procReadHead: string;
  procRootWrite: string;
  procRootWriteS: string;
  procRootWriteHead: string;
  procWrite: string;
  procWriteHead: string;
  procCheckempty: string;
  procCheckemptyS: string;
  procCheckemptyTbl: string;
  headPlplusGlobal: string;
  headPlplusLocal: string;
  struct: string;
  structItem: string;
  table: string;
}
export const defaultTemplates: Templates = {
  elemReadSimple: tStr(elemReadSimple),
  tableRead: tStr(tableRead),
  elemRead: tStr(elemRead),
  read: tStr(read),
  elemReadChoice: tStr(elemReadChoice),
  elemWriteSimple: tStr(elemWriteSimple),
  tableWrite: tStr(tableWrite),
  elemWrite: tStr(elemWrite),
  write: tStr(write),
  writeCheck: tStr(writeCheck),
  elemWriteChoice: tStr(elemWriteChoice),

  procRootRead: tStr(procRootRead),
  procRootReadS: shortRead(tStr(procRootRead)),
  procRootReadHead: getPlFuncHead(tStr(procRootRead)),
  procRead: tStr(procRead),
  procReadHead: getPlFuncHead(tStr(procRead)),
  procRootWrite: tStr(procRootWrite),
  procRootWriteS: shortWrite(tStr(procRootWrite)),
  procRootWriteHead: getPlFuncHead(tStr(procRootWrite)),
  procWrite: tStr(procWrite),
  procWriteHead: getPlFuncHead(tStr(procWrite)),
  procCheckempty: tStr(procCheckempty),
  procCheckemptyS: shortCheck(tStr(procCheckempty)),
  procCheckemptyTbl: tStr(procCheckemptyTbl),
  headPlplusGlobal: tStr(headPlplusGlobal),
  headPlplusLocal: tStr(headPlplusLocal),
  struct: tStr(struct),
  structItem: tStr(structItem),
  table: tStr(table)
};
export const getTemplates = ({
  elemReadSimple,
  tableRead,
  elemRead,
  read,
  elemReadChoice,
  elemWriteSimple,
  tableWrite,
  elemWrite,
  write,
  writeCheck,
  elemWriteChoice,
  procRootRead,
  procRead,
  procRootWrite,
  procWrite,
  procCheckempty,
  procCheckemptyTbl,
  headPlplusGlobal,
  headPlplusLocal,
  struct,
  structItem,
  table
}: Partial<Templates> = defaultTemplates): Templates => {
  return {
    elemReadSimple: elemReadSimple || defaultTemplates.elemReadSimple,
    tableRead: tableRead || defaultTemplates.tableRead,
    elemRead: elemRead || defaultTemplates.elemRead,
    read: read || defaultTemplates.read,
    elemReadChoice: elemReadChoice || defaultTemplates.elemReadChoice,
    elemWriteSimple: elemWriteSimple || defaultTemplates.elemWriteSimple,
    tableWrite: tableWrite || defaultTemplates.tableWrite,
    elemWrite: elemWrite || defaultTemplates.elemWrite,
    write: write || defaultTemplates.write,
    writeCheck: writeCheck || defaultTemplates.writeCheck,
    elemWriteChoice: elemWriteChoice || defaultTemplates.elemWriteChoice,
    procRootRead: procRootRead || defaultTemplates.procRootRead,
    procRootReadS: procRootRead
      ? shortRead(procRootRead)
      : defaultTemplates.procRootReadS,
    procRootReadHead: procRootRead
      ? getPlFuncHead(procRootRead)
      : defaultTemplates.procRootReadHead,
    procRead: procRead || defaultTemplates.procRead,
    procReadHead: procRead
      ? getPlFuncHead(procRead)
      : defaultTemplates.procReadHead,
    procRootWrite: procRootWrite || defaultTemplates.procRootWrite,
    procRootWriteS: procRootWrite
      ? shortWrite(procRootWrite)
      : defaultTemplates.procRootWriteS,
    procRootWriteHead: procRootWrite
      ? getPlFuncHead(procRootWrite)
      : defaultTemplates.procRootWriteHead,
    procWrite: procWrite || defaultTemplates.procWrite,
    procWriteHead: procWrite
      ? getPlFuncHead(procWrite)
      : defaultTemplates.procWriteHead,
    procCheckempty: procCheckempty || defaultTemplates.procCheckempty,
    procCheckemptyS: procCheckempty
      ? shortCheck(procCheckempty)
      : defaultTemplates.procCheckemptyS,
    headPlplusGlobal: headPlplusGlobal || defaultTemplates.headPlplusGlobal,
    headPlplusLocal: headPlplusLocal || defaultTemplates.headPlplusLocal,
    procCheckemptyTbl: procCheckemptyTbl || defaultTemplates.procCheckemptyTbl,
    struct: struct || defaultTemplates.struct,
    structItem: structItem || defaultTemplates.structItem,
    table: table || defaultTemplates.table
  };
};
