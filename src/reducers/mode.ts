import { Action, Reducer } from "redux";
import { GeneratorMode } from "../types";
export interface SetModeAction extends Action<"setGeneratorMode"> {
  payload: GeneratorMode;
}
export const reducer: Reducer<GeneratorMode, SetModeAction> = (
  state: GeneratorMode = GeneratorMode.plplus,
  action: SetModeAction
): GeneratorMode => {
  switch (action.type) {
    case "setGeneratorMode":
      return action.payload;
    default:
      return state;
  }
};
