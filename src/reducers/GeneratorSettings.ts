import { Reducer, combineReducers } from "redux";
import { globaldefaults as GenConst } from "../constantsDefaultGeneratorSettingsWrapped";
import { Templates, defaultTemplates, tStr } from "../utils/templates";
import { templates as reducerTemplates } from "./templates";
import { dataTypeTemplates as reducerDataTypeTemplates } from "./dataTypeTemplates";
import {
  DataTypeTemplates,
  defaultDataTypeTemplates
} from "../utils/TemplatesDataType";
import { SetSettingsAction } from "../actions";
export class GeneratorSettings {
  public constructor(
    public templates: Templates = defaultTemplates,
    public dataTypeTemplates: DataTypeTemplates = defaultDataTypeTemplates,
    public xsdField: string = tStr(GenConst.xsdField),
    public tbl: string = tStr(GenConst.tbl),
    public rqMask: string = tStr(GenConst.rqMask),
    public rsMask: string = tStr(GenConst.rsMask),
    public reservWords: string = tStr(GenConst.reservWords),
    public genRead: boolean = true,
    public genWrite: boolean = true
  ) {}
}
const genWrite = (
  state: boolean = true,
  action: SetSettingsAction<"genWrite">
): boolean => {
  switch (action.type) {
    case "genWrite":
      return action.payload;
    default:
      return state;
  }
};
const genRead = (
  state: boolean = true,
  action: SetSettingsAction<"genRead">
): boolean => {
  switch (action.type) {
    case "genRead":
      return action.payload;
    default:
      return state;
  }
};

const xsdField = (
  state: string = tStr(GenConst.xsdField),
  action: SetSettingsAction<"xsdField">
): string => {
  switch (action.type) {
    case "xsdField":
      return action.payload;
    default:
      return state;
  }
};
const tbl = (
  state: string = tStr(GenConst.tbl),
  action: SetSettingsAction<"tbl">
): string => {
  switch (action.type) {
    case "tbl":
      return action.payload;
    default:
      return state;
  }
};
const rqMask = (
  state: string = tStr(GenConst.rqMask),
  action: SetSettingsAction<"rqMask">
): string => {
  switch (action.type) {
    case "rqMask":
      return action.payload;
    default:
      return state;
  }
};
const rsMask = (
  state: string = tStr(GenConst.rsMask),
  action: SetSettingsAction<"rsMask">
): string => {
  switch (action.type) {
    case "rsMask":
      return action.payload;
    default:
      return state;
  }
};
const reservWords = (
  state: string = tStr(GenConst.reservWords),
  action: SetSettingsAction<"reservWords">
): string => {
  switch (action.type) {
    case "reservWords":
      return action.payload;
    default:
      return state;
  }
};
export const reducer: Reducer<GeneratorSettings> = combineReducers({
  templates: reducerTemplates as Reducer<Templates>,
  dataTypeTemplates: reducerDataTypeTemplates,
  xsdField,
  tbl,
  rqMask,
  rsMask,
  reservWords,
  genWrite,
  genRead
});
