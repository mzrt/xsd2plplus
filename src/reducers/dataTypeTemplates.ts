import {
  defaultDataTypeTemplates,
  DataTypeTemplates
} from "../utils/TemplatesDataType";
import { SetDataTypeTemplateValueAction } from "../actions/dataTypeTemplates";

export const dataTypeTemplates = (
  state: DataTypeTemplates = defaultDataTypeTemplates,
  action: SetDataTypeTemplateValueAction<keyof DataTypeTemplates>
): DataTypeTemplates => {
  switch (action.type) {
    case "funcReadString":
    case "funcWrite":
    case "funcReadNumber":
    case "funcReadBoolean":
    case "funcReadDate":
    case "funcWriteConvertDate":
      const retVal: DataTypeTemplates = {
        ...state,
        [action.type]: action.payload
      };
      return retVal;
    default:
      return state;
  }
};
