import { Reducer } from "redux";
import {
  AddFileAction,
  ChangeFileAction,
  ClearFileAction,
  RemoveFileAction,
  SetFilesAction,
  SetStartFileAction
} from "../actions/xsdFiles";
import { FileContentInfo } from "../models";

export const xsdFiles: Reducer<
  FileContentInfo[],
  | AddFileAction
  | RemoveFileAction
  | ChangeFileAction
  | ClearFileAction
  | SetFilesAction
> = (
  state: FileContentInfo[] = [],
  action:
    | AddFileAction
    | RemoveFileAction
    | ChangeFileAction
    | ClearFileAction
    | SetFilesAction
): FileContentInfo[] => {
  switch (action.type) {
    case "addFile":
      return state.slice(0).concat(action.payload);
    case "removeFile":
      return state
        .slice(0, action.payload)
        .concat(state.slice(action.payload + 1));
    case "changeFile":
      return state.map((item, index) =>
        action.payload.idx === index ? action.payload.file : item
      );
    case "clearFiles":
      return [];
    case "setFiles":
      return action.payload;
    default:
      return state;
  }
};

export const xsdStartFile: Reducer<string, SetStartFileAction> = (
  state: string = "",
  action: SetStartFileAction
): string => {
  switch (action.type) {
    case "setStartFile":
      return action.payload;
    default:
      return state;
  }
};
