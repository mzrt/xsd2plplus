import {
  Templates,
  defaultTemplates,
  shortCheck,
  shortRead,
  shortWrite,
  getPlFuncHead
} from "../utils/templates";
import { SetTemplateValueAction } from "../actions/templates";

export const templates = (
  state: Templates = defaultTemplates,
  action: SetTemplateValueAction<keyof Templates>
): Templates => {
  const shortTemplateProp: { prop: keyof Templates; val: string } | null =
    action.type === "procRootRead"
      ? {
          prop: "procRootReadS",
          val: shortRead(action.payload)
        }
      : action.type === "procRootWrite"
      ? { prop: "procRootWriteS", val: shortWrite(action.payload) }
      : action.type === "procCheckempty"
      ? { prop: "procCheckemptyS", val: shortCheck(action.payload) }
      : null;
  const headTemplateProp: { prop: keyof Templates; val: string } | null =
    action.type === "procRootRead" ||
    action.type === "procRead" ||
    action.type === "procRootWrite" ||
    action.type === "procWrite"
      ? {
          prop:
            action.type === "procRootRead"
              ? "procRootReadHead"
              : action.type === "procRead"
              ? "procReadHead"
              : action.type === "procRootWrite"
              ? "procRootWriteHead"
              : "procWriteHead",
          val: getPlFuncHead(action.payload)
        }
      : null;
  switch (action.type) {
    case "elemReadSimple":
    case "tableRead":
    case "elemRead":
    case "read":
    case "elemReadChoice":
    case "elemWriteSimple":
    case "tableWrite":
    case "elemWrite":
    case "write":
    case "writeCheck":
    case "elemWriteChoice":
    case "procRootRead":
    case "procRootReadS":
    case "procRootReadHead":
    case "procRead":
    case "procReadHead":
    case "procRootWrite":
    case "procRootWriteS":
    case "procRootWriteHead":
    case "procWrite":
    case "procWriteHead":
    case "procCheckempty":
    case "procCheckemptyS":
    case "procCheckemptyTbl":
    case "headPlplusGlobal":
    case "headPlplusLocal":
    case "struct":
    case "structItem":
    case "table":
      const retVal: Templates = { ...state, [action.type]: action.payload };
      if (shortTemplateProp) {
        retVal[shortTemplateProp.prop] = shortTemplateProp.val;
      }
      if (headTemplateProp) {
        retVal[headTemplateProp.prop] = headTemplateProp.val;
      }
      return retVal;
    default:
      return state;
  }
};
