import { combineReducers, Reducer } from "redux";
import { GeneratorSettings, reducer as settings } from "./GeneratorSettings";
import { reducer as mode } from "./mode";
import { GeneratorMode } from "../types";
import { FileContentInfo } from "../models";
import { xsdFiles, xsdStartFile } from "./xsdFiles";

export interface AppState {
  mode: GeneratorMode;
  settings: GeneratorSettings;
  xsdFiles: FileContentInfo[];
  xsdStartFile: string;
}

export function defaultState(): AppState {
  return {
    mode: GeneratorMode.plplus,
    settings: new GeneratorSettings(),
    xsdFiles: [],
    xsdStartFile: ""
  };
}

export const mainReducer: Reducer<AppState> = combineReducers({
  mode,
  settings,
  xsdFiles,
  xsdStartFile
});
