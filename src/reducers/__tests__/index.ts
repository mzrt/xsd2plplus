import { mainReducer, defaultState } from "../";

it("defaultState", () => {
  expect(defaultState()).toMatchSnapshot();
});

it("mainReducer", () => {
  expect(mainReducer(defaultState(), { type: "some" })).toMatchSnapshot();
});
