import { GeneratorSettings } from "../GeneratorSettings";
import * as GenConst from "../../constantsDefaultGeneratorSettingsWrapped";
import { defaultTemplates } from "../../utils/templates";
import { defaultDataTypeTemplates } from "../../utils/TemplatesDataType";
it("GeneratorSettings constructor", () => {
  expect(new GeneratorSettings()).toEqual({
    templates: defaultTemplates,
    dataTypeTemplates: defaultDataTypeTemplates,
    xsdField: GenConst.xsdField.join("\n"),
    tbl: GenConst.tbl.join("\n"),
    rqMask: GenConst.rqMask.join("\n"),
    rsMask: GenConst.rsMask.join("\n"),
    reservWords: GenConst.reservWords.join("\n"),
    genRead: true,
    genWrite: true
  });
});
