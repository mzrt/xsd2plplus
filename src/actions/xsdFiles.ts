import { Action } from "redux";
import { FileContentInfo } from "../models";
export interface AddFileAction extends Action<"addFile"> {
  payload: FileContentInfo;
}
export interface RemoveFileAction extends Action<"removeFile"> {
  payload: number;
}
export interface ChangeFileAction extends Action<"changeFile"> {
  payload: { idx: number; file: FileContentInfo };
}
export interface ClearFileAction extends Action<"clearFiles"> {
  payload: undefined;
}
export interface SetFilesAction extends Action<"setFiles"> {
  payload: FileContentInfo[];
}
export interface SetStartFileAction extends Action<"setStartFile"> {
  payload: string;
}

export const addFile = (file: FileContentInfo): AddFileAction => ({
  type: "addFile",
  payload: file
});
export const removeFile = (idx: number): RemoveFileAction => ({
  type: "removeFile",
  payload: idx
});
export const changeFile = (
  idx: number,
  file: FileContentInfo
): ChangeFileAction => ({
  type: "changeFile",
  payload: { idx, file }
});
export const clearFiles = (): ClearFileAction => ({
  type: "clearFiles",
  payload: undefined
});
export const setFiles = (files: FileContentInfo[]): SetFilesAction => ({
  type: "setFiles",
  payload: files
});
export const setStartFile = (idx: string): SetStartFileAction => ({
  type: "setStartFile",
  payload: idx
});
