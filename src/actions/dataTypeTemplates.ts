import { Action } from "redux";
import { DataTypeTemplates } from "../utils/TemplatesDataType";

export type DataTypeTemplatesProps = keyof DataTypeTemplates;

export interface SetDataTypeTemplateValueAction<
  T extends DataTypeTemplatesProps
> extends Action<T> {
  payload: DataTypeTemplates[T];
}
export const setProp = <T extends DataTypeTemplatesProps>(prop: T) => (
  value: DataTypeTemplates[T]
): SetDataTypeTemplateValueAction<T> => ({
  type: prop,
  payload: value
});
