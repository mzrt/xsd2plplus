import { GeneratorSettings } from "../reducers/GeneratorSettings";
import { Action } from "redux";
export { setProp as setPropTemplates } from "./templates";
export { setProp as setPropDataTypeTemplates } from "./dataTypeTemplates";
export {
  addFile,
  removeFile,
  changeFile,
  clearFiles,
  setFiles,
  setStartFile
} from "./xsdFiles";
export type SettingsProps = Exclude<
  keyof GeneratorSettings,
  "templates" | "dataTypeTemplates"
>;

export interface SetSettingsAction<T extends SettingsProps> extends Action<T> {
  payload: GeneratorSettings[T];
}
export const setProp = <T extends SettingsProps>(prop: T) => (
  value: GeneratorSettings[T]
): SetSettingsAction<T> => ({
  type: prop,
  payload: value
});
