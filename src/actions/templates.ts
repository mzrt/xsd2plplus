import { Action } from "redux";
import { Templates } from "../utils/templates";

export type TemplatesProps = keyof Templates;

export interface SetTemplateValueAction<T extends TemplatesProps>
  extends Action<T> {
  payload: Templates[T];
}
export const setProp = <T extends TemplatesProps>(prop: T) => (
  value: Templates[T]
): SetTemplateValueAction<T> => ({
  type: prop,
  payload: value
});
