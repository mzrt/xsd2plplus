import React, { ChangeEvent } from "react";
import PopoverButton from "./PopoverButton";
import XMLViewer from "react-xml-viewer";
import { FileContentInfo, FileInfo } from "../models";
import Radio from "@material-ui/core/Radio/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
export class ReadFiles extends React.Component<{
  onChangeFiles: (event: ChangeEvent<HTMLInputElement>) => void;
  files: FileInfo[];
  filesContent: FileContentInfo[][];
  setStartXsdFile: (zipIdx: number, entryIdx: string) => void;
  startXsd: (string | undefined)[];
}> {
  public handleChangeStartXsd = (zipIdx: number) => {
    return (event: ChangeEvent<HTMLInputElement>, checked: boolean): void => {
      const { setStartXsdFile } = this.props;
      setStartXsdFile(zipIdx, (event.target as HTMLInputElement).value);
    };
  };
  render(): JSX.Element {
    const { files, filesContent, onChangeFiles, startXsd } = this.props;
    return (
      <div>
        <input type={"file"} id="file" name="file" onChange={onChangeFiles} />
        <div>
          {files && files.length
            ? files.map(
                ({ file, dateStart, dateFinish, contentFiles }, zipIdx) => (
                  <div key={file.name}>
                    <h4>
                      {file.name}
                      {dateFinish ? (
                        <span className="small">
                          {" (loaded in " +
                            (dateFinish.getMilliseconds() -
                              dateStart.getMilliseconds()) +
                            "ms)"}
                        </span>
                      ) : null}
                    </h4>

                    <FormControl component="fieldset">
                      <FormLabel component="legend">
                        Выберите основной файл
                      </FormLabel>
                      <RadioGroup
                        aria-label="gender"
                        name="gender1"
                        value={startXsd[zipIdx]}
                      >
                        {contentFiles && contentFiles.length
                          ? contentFiles.map(
                              ({ zipEntry, relativePath }, entryIdx) => {
                                const entryInfo =
                                  filesContent[zipIdx] &&
                                  filesContent[zipIdx][entryIdx];
                                const entryValue = String(entryIdx);
                                return (
                                  <p>
                                    <FormControlLabel
                                      value={entryValue}
                                      control={
                                        <Radio
                                          checked={
                                            startXsd[zipIdx] === entryValue
                                          }
                                          onChange={this.handleChangeStartXsd(
                                            zipIdx
                                          )}
                                          value={entryValue}
                                          color="default"
                                          name="radio-button-demo"
                                          inputProps={{ "aria-label": "D" }}
                                        />
                                      }
                                      label={
                                        <div>
                                          {zipEntry.name +
                                            "(" +
                                            relativePath +
                                            ")"}
                                          {entryInfo ? (
                                            <PopoverButton
                                              title={
                                                <XMLViewer
                                                  xml={entryInfo.text}
                                                />
                                              }
                                            >
                                              {entryInfo.status === "ready"
                                                ? "XSD"
                                                : entryInfo.status === "loading"
                                                ? "Загружаем..."
                                                : "Неизвестное состояние"}
                                            </PopoverButton>
                                          ) : null}
                                        </div>
                                      }
                                    />
                                  </p>
                                );
                              }
                            )
                          : null}{" "}
                      </RadioGroup>
                    </FormControl>
                  </div>
                )
              )
            : null}
        </div>
      </div>
    );
  }
}
