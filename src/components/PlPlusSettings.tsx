import React from "react";
import TextField from "@material-ui/core/TextField";
import {
  Paper,
  Grid,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Typography
} from "@material-ui/core";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { GeneratorSettings } from "../reducers/GeneratorSettings";
import { Templates } from "../utils/templates";
import { DataTypeTemplates } from "../utils/TemplatesDataType";
import { TemplatesProps } from "../actions/templates";
import { SettingsProps } from "../actions";
import { DataTypeTemplatesProps } from "../actions/dataTypeTemplates";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      flexWrap: "wrap"
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    paper: {
      padding: theme.spacing(2),
      color: theme.palette.text.primary,
      marginBottom: theme.spacing(1)
    },
    button: {
      margin: theme.spacing(1)
    }
  })
);
export interface PlPlusSettingsProps {
  settings?: GeneratorSettings;

  onChangeDataTypeTemplate: <T extends DataTypeTemplatesProps>(
    prop: T
  ) => (value: DataTypeTemplates[T]) => void;
  onChangeSettings: <T extends SettingsProps>(
    prop: T
  ) => (value: GeneratorSettings[T]) => void;
  onChangeTemplate: <T extends TemplatesProps>(
    prop: T
  ) => (value: Templates[T]) => void;
}
export const PlPlusGeneratorOptions = (
  props: PlPlusSettingsProps = {
    settings: new GeneratorSettings(),
    onChangeDataTypeTemplate: <T extends DataTypeTemplatesProps>(prop: T) => (
      value: DataTypeTemplates[T]
    ): void => {},
    onChangeSettings: <T extends SettingsProps>(prop: T) => (
      value: GeneratorSettings[T]
    ): void => {},
    onChangeTemplate: <T extends TemplatesProps>(prop: T) => (
      value: Templates[T]
    ): void => {}
  }
): JSX.Element => {
  const classes = useStyles();
  const {
    settings: {
      genRead,
      genWrite,
      templates: {
        elemReadSimple,
        tableRead,
        elemRead,
        read,
        elemReadChoice,
        elemWriteSimple,
        tableWrite,
        elemWrite,
        write,
        writeCheck,
        elemWriteChoice,
        procRootRead,
        procRead,
        procRootWrite,
        procWrite,
        procCheckempty,
        procCheckemptyTbl,
        headPlplusGlobal,
        headPlplusLocal,
        struct,
        structItem,
        table
      },
      dataTypeTemplates: {
        funcReadBoolean,
        funcReadDate,
        funcReadNumber,
        funcReadString,
        funcWrite,
        funcWriteConvertDate
      },
      reservWords
    } = new GeneratorSettings(),
    onChangeDataTypeTemplate,
    onChangeSettings,
    onChangeTemplate
  } = props;
  const onChangeDataTypeTemplateLocal = <T extends DataTypeTemplatesProps>(
    prop: T
  ): ((event: React.ChangeEvent<HTMLInputElement>) => void) => (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    if (onChangeDataTypeTemplate) {
      onChangeDataTypeTemplate(prop)(event.target.value);
    }
  };
  const onChangeSettingsLocal = <T extends SettingsProps>(
    prop: T
  ): ((event: React.ChangeEvent<HTMLInputElement>) => void) => (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    if (onChangeSettings) {
      if (prop === "genRead" || prop === "genWrite") {
        onChangeSettings(prop as "genRead" | "genWrite")(event.target.checked);
      } else {
        onChangeSettings(
          prop as Exclude<SettingsProps, "genRead" | "genWrite">
        )(event.target.value);
      }
    }
  };
  const onChangeTemplateLocal = <T extends TemplatesProps>(
    prop: T
  ): ((event: React.ChangeEvent<HTMLInputElement>) => void) => (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    if (onChangeTemplate) {
      onChangeTemplate(prop)(event.target.value);
    }
  };
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Paper className={classes.paper} key="define">
          <Grid container spacing={2}>
            <Grid item md={6} xs={12} key="header">
              <TextField
                id="headPlplusGlobal"
                onChange={onChangeTemplateLocal("headPlplusGlobal")}
                label="Заголовок pl/plus global"
                rows="4"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                key="global"
                value={headPlplusGlobal}
              />
              <TextField
                id="headPlplusLocal"
                onChange={onChangeTemplateLocal("headPlplusLocal")}
                label="Заголовок pl/plus local"
                rows="20"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                key="local"
                value={headPlplusLocal}
              />
            </Grid>
            <Grid item md={6} xs={12} key="struct">
              <TextField
                label="Структура pl/plus"
                id="struct"
                onChange={onChangeTemplateLocal("struct")}
                rows="4"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                key="struct"
                value={struct}
              />
              <TextField
                label="Реквизит структуры"
                id="structItem"
                onChange={onChangeTemplateLocal("structItem")}
                rows="2"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                key="items"
                value={structItem}
              />
              <TextField
                label="Таблица pl/plus"
                id="table"
                onChange={onChangeTemplateLocal("table")}
                rows="4"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                key="table"
                value={table}
              />
              <TextField
                label="Зарезервированные слова pl/plus"
                id="reservWords"
                onChange={onChangeSettingsLocal("reservWords")}
                rows="7"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                key="reservWords"
                value={reservWords}
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper} key="rootFunc">
          <Grid container spacing={2}>
            <Grid item md={6} xs={12} key="read">
              <Grid container key="header">
                <Grid item md={6} xs={12} key="title">
                  <Typography variant="h6" gutterBottom>
                    Операция чтения корневого типа:
                  </Typography>
                </Grid>
                <Grid item md={6} xs={12} key="genCheckBox">
                  <FormGroup row key="genRead">
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={genRead}
                          onChange={onChangeSettingsLocal("genRead")}
                          value="genRead"
                          color="primary"
                        />
                      }
                      label="Генерировать"
                    />
                  </FormGroup>
                </Grid>
              </Grid>
              <TextField
                id="procRootRead"
                onChange={onChangeTemplateLocal("procRootRead")}
                rows="14"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={procRootRead}
              />
            </Grid>
            <Grid item md={6} xs={12} key="write">
              <Grid container key="header">
                <Grid item md={6} xs={12} key="title">
                  <Typography variant="h6" gutterBottom>
                    Операция записи корневого типа:
                  </Typography>
                </Grid>
                <Grid item md={6} xs={12} key="genCheckBox">
                  <FormGroup row key="genWrite">
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={genWrite}
                          onChange={onChangeSettingsLocal("genWrite")}
                          value="genWrite"
                          color="primary"
                        />
                      }
                      label="Генерировать"
                    />
                  </FormGroup>
                </Grid>
              </Grid>
              <TextField
                id="procRootWrite"
                onChange={onChangeTemplateLocal("procRootWrite")}
                rows="14"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={procRootWrite}
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper} key="nestedFunc">
          <Grid container spacing={2}>
            <Grid item md={6} xs={12} key="read">
              <TextField
                label="Операция чтения вложенного типа"
                id="procRead"
                onChange={onChangeTemplateLocal("procRead")}
                rows="10"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={procRead}
              />
            </Grid>
            <Grid item md={6} xs={12} key="write">
              <TextField
                label="Операция записи вложенного типа"
                id="procWrite"
                onChange={onChangeTemplateLocal("procWrite")}
                rows="10"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={procWrite}
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper} key="check">
          <Grid container spacing={2}>
            <Grid item md={6} xs={12} key="table">
              <TextField
                label="Операция проверки на пустое значение табличного типа"
                id="procCheckemptyTbl"
                onChange={onChangeTemplateLocal("procCheckemptyTbl")}
                rows="12"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={procCheckemptyTbl}
              />
            </Grid>
            <Grid item md={6} xs={12} key="child">
              <TextField
                label="Операция проверки на пустое значение вложенного типа"
                id="procCheckempty"
                onChange={onChangeTemplateLocal("procCheckempty")}
                rows="12"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={procCheckempty}
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper} key="simpleItem">
          <Grid container spacing={2}>
            <Grid item md={6} xs={12} key="templateRead">
              <TextField
                label="Чтение элемента простого типа"
                id="elemReadSimple"
                onChange={onChangeTemplateLocal("elemReadSimple")}
                rows="3"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={elemReadSimple}
              />
            </Grid>
            <Grid item md={6} xs={12} key="templateWrite">
              <TextField
                label="Запись элемента простого типа"
                id="elemWriteSimple"
                onChange={onChangeTemplateLocal("elemWriteSimple")}
                rows="3"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={elemWriteSimple}
              />
            </Grid>
            <Grid item md={6} xs={12} key="read">
              <Grid container key="simpleTypes" spacing={2}>
                <Grid item xs={12} key="title">
                  <Typography variant="body1" gutterBottom>
                    Операции чтения реквизитов простых типов:
                  </Typography>
                </Grid>
                <Grid item md={6} xs={12} key="String">
                  <TextField
                    label="Строка"
                    id="readString"
                    margin="normal"
                    variant="outlined"
                    fullWidth={true}
                    value={funcReadString}
                    onChange={onChangeDataTypeTemplateLocal("funcReadString")}
                  />
                </Grid>
                <Grid item md={6} xs={12} key="Number">
                  <TextField
                    label="Число"
                    id="readNumber"
                    margin="normal"
                    variant="outlined"
                    fullWidth={true}
                    value={funcReadNumber}
                    onChange={onChangeDataTypeTemplateLocal("funcReadNumber")}
                  />
                </Grid>
                <Grid item md={6} xs={12} key="Date">
                  <TextField
                    label="Дата"
                    id="readDate"
                    margin="normal"
                    variant="outlined"
                    fullWidth={true}
                    value={funcReadDate}
                    onChange={onChangeDataTypeTemplateLocal("funcReadDate")}
                  />
                </Grid>
                <Grid item md={6} xs={12} key="Boolean">
                  <TextField
                    label="Логика"
                    id="readBoolean"
                    margin="normal"
                    variant="outlined"
                    fullWidth={true}
                    value={funcReadBoolean}
                    onChange={onChangeDataTypeTemplateLocal("funcReadBoolean")}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item md={6} xs={12} key="write">
              <Grid container key="simpleTypes" spacing={2}>
                <Grid item xs={12} key="title">
                  <Typography variant="body1" gutterBottom>
                    Операции записи реквизитов простых типов:
                  </Typography>
                </Grid>
                <Grid item xs={12} key="allTypesWrite">
                  <TextField
                    label="Операция записи реквизитов простых типов"
                    id="writeValOper"
                    margin="normal"
                    variant="outlined"
                    fullWidth={true}
                    value={funcWrite}
                    onChange={onChangeDataTypeTemplateLocal("funcWrite")}
                  />
                </Grid>
                <Grid item xs={12} key="dateConvert">
                  <TextField
                    label="Обернуть дату в вызов функции"
                    id="funcWriteConvertDate"
                    margin="normal"
                    variant="outlined"
                    fullWidth={true}
                    value={funcWriteConvertDate}
                    onChange={onChangeDataTypeTemplateLocal(
                      "funcWriteConvertDate"
                    )}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper} key="complexTypeFunc">
          <Grid container spacing={2}>
            <Grid item md={4} xs={12} key="read">
              <TextField
                label="Чтение сложного типа"
                id="read"
                onChange={onChangeTemplateLocal("read")}
                value={read}
                rows="7"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
              />
            </Grid>
            <Grid item md={4} xs={12} key="write">
              <TextField
                label="Запись сложного типа"
                id="write"
                onChange={onChangeTemplateLocal("write")}
                value={write}
                rows="7"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
              />
            </Grid>
            <Grid item md={4} xs={12} key="checkWrite">
              <TextField
                label="Проверка необязательного тега на пустое значение перед записью"
                id="writeCheck"
                onChange={onChangeTemplateLocal("writeCheck")}
                value={writeCheck}
                rows="7"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper} key="complexItem">
          <Grid container spacing={2}>
            <Grid item md={6} xs={12} key="read">
              <TextField
                label="Чтение элемента сложного типа"
                id="elemRead"
                onChange={onChangeTemplateLocal("elemRead")}
                value={elemRead}
                rows="3"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
              />
            </Grid>
            <Grid item md={6} xs={12} key="write">
              <TextField
                label="Запись элемента сложного типа"
                id="elemWrite"
                onChange={onChangeTemplateLocal("elemWrite")}
                value={elemWrite}
                rows="3"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper} key="table">
          <Grid container spacing={2}>
            <Grid item md={6} xs={12} key="read">
              <TextField
                label="Чтение таблицы"
                id="tableRead"
                rows="9"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={tableRead}
                onChange={onChangeTemplateLocal("tableRead")}
              />
            </Grid>
            <Grid item md={6} xs={12} key="write">
              <TextField
                label="Запись таблицы"
                id="tableWrite"
                rows="9"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={tableWrite}
                onChange={onChangeTemplateLocal("tableWrite")}
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper} key="choice">
          <Grid container spacing={2}>
            <Grid item md={6} xs={12} key="read">
              <TextField
                label="Чтение choice"
                id="elemReadChoice"
                rows="4"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={elemReadChoice}
                onChange={onChangeTemplateLocal("elemReadChoice")}
              />
            </Grid>
            <Grid item md={6} xs={12} key="write">
              <TextField
                label="Запись choice"
                id="elemWriteChoice"
                rows="4"
                multiline={true}
                margin="normal"
                variant="outlined"
                fullWidth={true}
                value={elemWriteChoice}
                onChange={onChangeTemplateLocal("elemWriteChoice")}
              />
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
};
