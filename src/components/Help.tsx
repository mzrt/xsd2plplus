import React from "react";
import { Paper, Grid, Typography } from "@material-ui/core";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      flexWrap: "wrap"
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    paper: {
      padding: theme.spacing(2),
      color: theme.palette.text.primary,
      marginBottom: theme.spacing(1)
    },
    button: {
      margin: theme.spacing(1)
    }
  })
);
const Help = (): JSX.Element => {
  const classes = useStyles();
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <Grid container>
            <Grid item xs={12}>
              <Typography variant="h6" gutterBottom>
                <span>
                  1.<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                </span>
                Получить табличное представление схемы:
              </Typography>
              <Typography variant="body1" gutterBottom>
                Открываем{" "}
                <a href="http://10.68.199.84:8085/">
                  http://10.68.199.84:8085/
                </a>
                , «Документация схемы 4.0», «Выберите файл», «Обзор», выбрать
                xsd-файл, «Загрузить».
              </Typography>
              <Typography variant="body1" gutterBottom>
                Важное замечание, флаг «Таблица простых типов» не должен быть
                установлен.
              </Typography>
              <Typography variant="body1" gutterBottom>
                Перейти по ссылке в результатах, нажать «выделить все таблицы»,
                скопировать в буфер обмена и в ставить в документ Word или
                Excel. <br />
                это необходимо для того, чтобы получить текстовое представление
                таблицы с символом табуляции в качестве разделителя колонок.
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <Grid container>
            <Grid item xs={12}>
              <Typography variant="h6" gutterBottom>
                <span>
                  2.<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                </span>
                Сконвертировать в pl/plus:
              </Typography>

              <Typography variant="body1" gutterBottom>
                Выделить скопированные таблицы из Word/Excel и скопировать в
                буфер обмена.
                <br />
                Открыть конвертер (xsdconvert.html), очистить поле от данных
                примера, вставить в него содержимое буфера обмена.
              </Typography>

              <Typography variant="body1" gutterBottom>
                <span>
                  <img alt="" src="./images/image001.jpg" />
                </span>
              </Typography>

              <Typography variant="body1" gutterBottom></Typography>

              <Typography variant="body1" gutterBottom>
                Можно задать настройки для генерации операций чтения/записи
                запроса, с фильтром по маске корневого тега запроса.
              </Typography>

              <Typography variant="body1" gutterBottom>
                Нажать обработать.{" "}
              </Typography>

              <Typography variant="body1" gutterBottom>
                <span>
                  <img alt="" src="./images/image002.png" />
                </span>
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <Grid container>
            <Grid item xs={12}>
              <Typography variant="h6" gutterBottom>
                <span>
                  3.<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                </span>
                Результат:
              </Typography>

              <Typography variant="body1" gutterBottom>
                После обработки откроется вкладка результат, с двумя полями
                «Глобальная секция», «Локальная секция» с pl/plus-кодом
                объявления структур и операций записи/чтения/проверки на пустое
                значение.
              </Typography>

              <Typography variant="body1" gutterBottom>
                <span>
                  <img alt="" src="./images/image003.png" />
                </span>
              </Typography>

              <Typography variant="body1" gutterBottom>
                Нажатие на кнопки «Скопировать» скопирует содержимое поля в
                буфер обмена.
              </Typography>

              <Typography variant="body1" gutterBottom></Typography>

              <Typography variant="body1" gutterBottom>
                Для однотипных запросов (для корневых тегов) будет использован
                объявление типа со ссылкой на структуру первого реализованного
                запроса и перевызов операций чтения/записи/проверки.
              </Typography>

              <Typography variant="body1" gutterBottom>
                Для вложенных однотипных тегов будет использована структура
                созданная первой.
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
};
export default Help;
