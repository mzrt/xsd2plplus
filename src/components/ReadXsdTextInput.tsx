import React from "react";
import { FileContentInfo } from "../models";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Grid from "@material-ui/core/Grid";
import InputTextFile from "./InputTextFile";
import {
  Button,
  createStyles,
  makeStyles,
  Paper,
  Theme
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      flexWrap: "wrap"
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    paper: {
      padding: theme.spacing(1),
      color: theme.palette.text.secondary,
      marginBottom: theme.spacing(1)
    },
    button: {
      margin: theme.spacing(3)
    }
  })
);

export interface ReadXsdTextInputProps {
  files: FileContentInfo[];
  startXsd: string | undefined;
}
export interface ReadXsdTextInputPropsFunc {
  setStartXsdFile: (entryIdx: string) => void;
  addFile?: (file: FileContentInfo) => void;
  removeFile?: (idx: number) => void;
  changeFile?: (idx: number, file: FileContentInfo) => void;
  clearFiles?: () => void;
  setFiles?: (files: FileContentInfo[]) => void;
}
export function ReadXsdTextInput(
  props: ReadXsdTextInputProps & ReadXsdTextInputPropsFunc
): JSX.Element {
  const classes = useStyles();
  const handleChangeStartXsd = (value: string): void => {
    const { setStartXsdFile } = props;
    setStartXsdFile && setStartXsdFile(value);
  };
  const onChangeFile = (idx: number) => (file: FileContentInfo): void => {
    const { changeFile } = props;
    changeFile && changeFile(idx, file);
  };
  const { files, startXsd } = props;
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <Button
            id="process"
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={(): void =>
              props.addFile &&
              props.addFile({
                filename: "",
                relativePath: "",
                text: "",
                status: "ready"
              })
            }
          >
            Добавить файл
          </Button>
          <Button
            id="clearAll"
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={(): void => props.clearFiles && props.clearFiles()}
          >
            Удалить все
          </Button>
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Grid container wrap="nowrap" spacing={2}>
          <FormControl component="fieldset">
            {files && files.length ? (
              <FormLabel component="legend">Выберите основной файл</FormLabel>
            ) : null}
            {files && files.length
              ? files.map((file, entryIdx) => {
                  const entryValue = String(entryIdx);
                  return (
                    <Grid item xs>
                      <Grid container wrap="nowrap" spacing={2}>
                        <Grid item xs={10}>
                          <InputTextFile
                            checked={startXsd === entryValue}
                            onChangeValue={handleChangeStartXsd}
                            onChangeFile={onChangeFile(entryIdx)}
                            value={entryValue}
                            file={file}
                            title={
                              file.filename + "(" + file.relativePath + ")"
                            }
                          />
                        </Grid>
                        <Grid item xs={2}>
                          <Grid container wrap="nowrap" spacing={2}>
                            <Grid item xs wrap="nowrap">
                              <Button
                                id="delete"
                                variant="contained"
                                color="primary"
                                className={classes.button}
                                onClick={(): void =>
                                  props.removeFile && props.removeFile(entryIdx)
                                }
                              >
                                Удалить
                              </Button>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  );
                })
              : null}{" "}
          </FormControl>
        </Grid>
      </Grid>
    </Grid>
  );
}
