import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import React, { ChangeEvent } from "react";
import { FileContentInfo } from "../models";
import {
  createStyles,
  FormGroup,
  Grid,
  Paper,
  TextField,
  Theme
} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      flexWrap: "wrap"
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    paper: {
      padding: theme.spacing(1),
      color: theme.palette.text.secondary,
      marginBottom: theme.spacing(1)
    },
    button: {
      margin: theme.spacing(3)
    }
  })
);

export interface InputTextFileProps {
  title: any;
  checked: boolean;
  value: string;
  onChangeValue: (newValu: string) => void;
  onChangeFile: (file: FileContentInfo) => void;
  file: FileContentInfo;
}
export default function InputTextFile(props: InputTextFileProps): JSX.Element {
  const classes = useStyles();
  const {
    checked,
    value,
    onChangeValue,
    file: { filename, relativePath, text }
  } = props;
  const onChange = (
    event: ChangeEvent<HTMLInputElement>,
    _checked: boolean
  ): void => {
    onChangeValue((event.target as HTMLInputElement).value);
  };

  const onChangeFileProp = (propName: keyof FileContentInfo) => (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    props.onChangeFile &&
      props.onChangeFile({
        ...props.file,
        [propName]: event.target.value
      });
  };
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} wrap="nowrap">
        <Paper className={classes.paper}>
          <Grid container spacing={2}>
            <Grid item xs={6} wrap="nowrap">
              <FormGroup row>
                <FormControlLabel
                  value={value}
                  control={
                    <Radio
                      checked={checked}
                      onChange={onChange}
                      value={value}
                      color="default"
                      name="radio-button-file"
                      inputProps={{ "aria-label": "D" }}
                    />
                  }
                  label={
                    <TextField
                      id="outlined-filename"
                      label="Имя файла"
                      className={classes.textField}
                      value={filename}
                      onChange={onChangeFileProp("filename")}
                      margin="normal"
                      variant="outlined"
                      fullWidth={true}
                    />
                  }
                />
              </FormGroup>
            </Grid>
            <Grid item xs={6}>
              <FormGroup row>
                <TextField
                  id="outlined-path"
                  label="Путь до файла"
                  className={classes.textField}
                  value={relativePath}
                  onChange={onChangeFileProp("relativePath")}
                  margin="normal"
                  variant="outlined"
                  fullWidth={true}
                />
              </FormGroup>
            </Grid>
            <Grid item xs={12}>
              <FormGroup row>
                <TextField
                  id="outlined-path"
                  label="Текст файла"
                  className={classes.textField}
                  value={text}
                  onChange={onChangeFileProp("text")}
                  margin="normal"
                  variant="outlined"
                  multiline
                  rowsMax={7}
                  fullWidth={true}
                />
              </FormGroup>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}
