import React from "react";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import { GeneratorSettings } from "../reducers/GeneratorSettings";
import TextField from "@material-ui/core/TextField";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
  Grid,
  Paper,
  Button,
  Typography,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Chip,
  Box
} from "@material-ui/core";
import { versionScript } from "./ChangeLog";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      flexWrap: "wrap"
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    paper: {
      padding: theme.spacing(1),
      color: theme.palette.text.secondary,
      marginBottom: theme.spacing(1)
    },
    button: {
      margin: theme.spacing(1)
    }
  })
);
export interface ApplyMaskState {
  apply: boolean;
  mask: string;
  forRead: boolean;
  forWrite: boolean;
}
export const ApplyMask = ({
  mask,
  maskNumber,
  onChangeMask
}: {
  mask: ApplyMaskState;
  maskNumber: 1 | 2;
  onChangeMask?: (
    name: keyof ApplyMaskState,
    maskNumber: 1 | 2
  ) => (event: React.ChangeEvent<HTMLInputElement>) => void;
}): JSX.Element => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Grid container wrap="nowrap" spacing={2}>
        <Grid item xs>
          <FormGroup row>
            <FormControlLabel
              control={
                <Checkbox
                  checked={mask && mask.apply}
                  onChange={onChangeMask && onChangeMask("apply", maskNumber)}
                  value="applyMask"
                  color="primary"
                />
              }
              label="По маске"
            />
          </FormGroup>
        </Grid>
      </Grid>
      <Grid container wrap="nowrap" spacing={2}>
        <Grid item xs>
          <FormGroup row>
            <TextField
              id="outlined-name"
              label="Маска"
              className={classes.textField}
              value={mask && mask.mask}
              onChange={onChangeMask && onChangeMask("mask", maskNumber)}
              margin="normal"
              variant="outlined"
              fullWidth={true}
              disabled={!mask.apply}
            />
          </FormGroup>
        </Grid>
      </Grid>
      <Grid container wrap="nowrap" spacing={2}>
        <Grid item xs={6}>
          <FormGroup row>
            <FormControlLabel
              control={
                <Checkbox
                  checked={mask && mask.forRead}
                  onChange={onChangeMask && onChangeMask("forRead", maskNumber)}
                  value="read"
                  color="primary"
                />
              }
              label="Генерировать чтение"
              disabled={!mask.apply}
            />
          </FormGroup>
        </Grid>
        <Grid item xs={6}>
          <FormGroup row>
            <FormControlLabel
              control={
                <Checkbox
                  checked={mask && mask.forWrite}
                  onChange={
                    onChangeMask && onChangeMask("forWrite", maskNumber)
                  }
                  value="write"
                  color="primary"
                />
              }
              label="Генерировать запись"
              disabled={!mask.apply}
            />
          </FormGroup>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};
// < !--Pl / Plus опции-- >
export const defaultMask: ApplyMaskState = {
  apply: false,
  forRead: false,
  forWrite: false,
  mask: ""
};
export const PlplusOptionsForm = ({
  mask1,
  mask2,
  onChangeMask
}: {
  mask1?: ApplyMaskState;
  mask2?: ApplyMaskState;
  onChangeMask?: (val: ApplyMaskState, maskNumber: 1 | 2) => void;
}): JSX.Element => {
  const classes = useStyles();
  const onChangeMaskLocal = (
    name: keyof ApplyMaskState,
    maskNumber: 1 | 2
  ): ((event: React.ChangeEvent<HTMLInputElement>) => void) => (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    if (onChangeMask) {
      let newVal: ApplyMaskState =
        (maskNumber === 1 ? mask1 : mask2) || defaultMask;
      if (name === "apply" || name === "forRead" || name === "forWrite") {
        newVal = { ...newVal, [name]: event.target.checked };
      }
      if (name === "mask") {
        newVal = { ...newVal, [name]: event.target.value };
      }
      onChangeMask(newVal, maskNumber);
    }
  };
  return (
    <React.Fragment>
      <Paper className={classes.paper}>
        <ApplyMask
          mask={mask1 || defaultMask}
          maskNumber={1}
          onChangeMask={onChangeMaskLocal}
        />
      </Paper>
      <Paper className={classes.paper}>
        <ApplyMask
          mask={mask2 || defaultMask}
          maskNumber={2}
          onChangeMask={onChangeMaskLocal}
        />
      </Paper>
      <Paper className={classes.paper}>
        <Grid container wrap="nowrap" spacing={2}>
          <Grid item xs>
            <Typography component="p">
              Для корректного определения размера текстовых реквизитов,
              необходиомо чтобы в колонке "Тип" дополнтительно к тексту
              "xs:string" был указан текст maxLength=&lt;размер&gt; или pattern
              с регулярным выражением дающим возможность определить размер,
              иначе будет взят максимальный размер для Oracle - 32767.
              Приведенный ниже текст указан только в качестве примера.
            </Typography>
          </Grid>
        </Grid>
      </Paper>
    </React.Fragment>
  );
};
export const XsdOptionsForm = (): JSX.Element => (
  <div id="xsd_options">Опции xsd</div>
);

export const UcsXsdOptionsForm = (): JSX.Element => (
  <div id="ucs_xsd_options">Опции ЕКС xsd</div>
);

export interface OptionProps {
  settings: GeneratorSettings;
  mask1?: ApplyMaskState;
  mask2?: ApplyMaskState;
  onChangeMask1: (val: ApplyMaskState) => void;
  onChangeMask2: (val: ApplyMaskState) => void;
  onChangeXsdTbl: (value: string) => void;
  runGenerator: () => void;
}
export const GeneratorOptions = (props: OptionProps): JSX.Element => {
  const classes = useStyles();
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { onChangeXsdTbl } = props;
    if (onChangeXsdTbl) {
      onChangeXsdTbl(event.target.value);
    }
  };
  const {
    settings = new GeneratorSettings(),
    onChangeMask1,
    onChangeMask2
  } = props;
  return (
    <Grid container spacing={3}>
      <Grid item xs={9}>
        <form className={classes.container} noValidate autoComplete="off">
          <TextField
            id="outlined-name"
            label="Текст табличного предстваления xsd-схемы"
            className={classes.textField}
            value={settings.tbl}
            onChange={handleChange}
            margin="normal"
            variant="outlined"
            multiline
            rows={37}
            fullWidth={true}
          />
          <Box
            position="relative"
            zIndex="modal"
            left="48px"
            bottom="8px"
            marginLeft="-40px"
          >
            <Chip size="small" label={"Версия " + versionScript} />
          </Box>
        </form>
      </Grid>
      <Grid item xs={3}>
        <Paper className={classes.paper}>
          <Button
            id="process"
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={props.runGenerator}
          >
            <PlayArrowIcon />
            Обработать
          </Button>
        </Paper>
        <PlplusOptionsForm
          mask1={props.mask1}
          mask2={props.mask2}
          onChangeMask={(val: ApplyMaskState, maskNumber: 1 | 2): void => {
            maskNumber === 1 ? onChangeMask1(val) : onChangeMask2(val);
          }}
        />
      </Grid>
    </Grid>
  );
};
