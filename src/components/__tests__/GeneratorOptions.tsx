import React from "react";
import { shallow } from "enzyme";
import { GeneratorOptions } from "../GeneratorOptions";
import { GeneratorSettings } from "../../reducers/GeneratorSettings";

it("renders without crashing", () => {
  shallow(
    <GeneratorOptions
      settings={new GeneratorSettings()}
      onChangeXsdTbl={(value): void => {}}
      runGenerator={(): void => {}}
      onChangeMask1={(): void => {}}
      onChangeMask2={(): void => {}}
    />
  );
  expect(
    shallow(
      <GeneratorOptions
        settings={new GeneratorSettings()}
        onChangeXsdTbl={(value): void => {}}
        runGenerator={(): void => {}}
        onChangeMask1={(): void => {}}
        onChangeMask2={(): void => {}}
      />
    )
  ).toMatchSnapshot();
});
