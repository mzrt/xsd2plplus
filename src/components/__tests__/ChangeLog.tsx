import React from "react";
import { shallow } from "enzyme";
import ChangeLog from "../ChangeLog";
import { Row, Col } from "react-bootstrap";
import { Grid } from "@material-ui/core";

it("renders without crashing", () => {
  shallow(<ChangeLog />);
  expect(shallow(<ChangeLog />)).toMatchSnapshot();
});

it("renders welcome message", () => {
  const wrapper = shallow(<ChangeLog />);
  const versionHeader = (
    <Grid item sm={10} xs={12} key="version">
      {"0.0.13"}
    </Grid>
  );
  const versionDate = (
    <Grid item sm={2} xs={12} key="date">
      {"15.10.2018"}
    </Grid>
  );
  expect(wrapper).toContainReact(versionHeader);
  expect(wrapper).toContainReact(versionDate);
});
