import React from "react";
import { shallow } from "enzyme";
import Help from "../Help";
import { render } from "@testing-library/react";

it("renders welcome message", () => {
  const { getByText } = render(<Help />);
  expect(getByText("Нажать обработать.")).toBeInTheDocument();
});

it("renders without crashing", () => {
  shallow(<Help />);
  expect(shallow(<Help />)).toMatchSnapshot();
});
