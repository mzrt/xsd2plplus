import React, { useRef, useState } from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
  Grid,
  FormGroup,
  Paper,
  Box,
  IconButton,
  Tooltip
} from "@material-ui/core";
import AssignmentIcon from "@material-ui/icons/Assignment";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      flexWrap: "wrap"
    },
    iconButton: {
      padding: 10
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    paperButton: {
      padding: theme.spacing(1),
      display: "flex",
      alignItems: "top"
      // width: 800,
    },
    paper: {
      padding: theme.spacing(1),
      color: theme.palette.text.secondary,
      marginBottom: theme.spacing(1)
    },
    button: {
      margin: theme.spacing(1)
    }
  })
);

export interface PlPlusResultProps {
  globalPlPlus: string;
  localPlPlus: string;
}
export const TextFieldButton = (props: any): JSX.Element => {
  const classes = useStyles();
  const { propsButton, ...restProps } = props;
  const [copySuccess, setCopySuccess] = useState("");
  const textAreaRef = useRef();

  const copyToClipboard = (e: any): void => {
    if (textAreaRef && textAreaRef.current) {
      debugger;
      (textAreaRef.current || { select: () => {} }).select();
      document.execCommand("copy");
    }
    // This is just personal preference.
    // I prefer to not show the the whole text area selected.
    e.target.focus();
    setCopySuccess("Скопировано!");
    setTimeout(() => setCopySuccess(""), 1500);
  };
  return (
    <Grid item xs={12}>
      <Paper className={classes.paperButton}>
        <TextField
          {...restProps}
          inputProps={{
            ref: textAreaRef
          }}
        />
        {document.queryCommandSupported("copy") && (
          <Box
            position="relative"
            zIndex="modal"
            right="80px"
            top="24px"
            marginRight="-40px"
          >
            <Tooltip title="Скопировать" placement="top">
              <IconButton
                color="default"
                className={classes.iconButton}
                aria-label="directions"
                onClick={copyToClipboard}
              >
                {propsButton &&
                  propsButton.iconButtonChilds &&
                  propsButton.iconButtonChilds()}
              </IconButton>
            </Tooltip>
            {copySuccess}
          </Box>
        )}
      </Paper>
    </Grid>
  );
};
export const PlPlusResult = (props: PlPlusResultProps): JSX.Element => {
  const classes = useStyles();
  const { globalPlPlus, localPlPlus } = props;
  return (
    <Grid container spacing={3}>
      <Grid item xs={6}>
        <FormGroup row>
          <TextFieldButton
            id="global"
            label="Глобальная секция"
            className={classes.textField}
            value={globalPlPlus}
            margin="normal"
            variant="outlined"
            multiline
            rows={37}
            fullWidth={true}
            propsButton={{ iconButtonChilds: () => <AssignmentIcon /> }}
          />
        </FormGroup>
      </Grid>
      <Grid item xs={6}>
        <FormGroup row>
          <TextFieldButton
            id="local"
            label="Локальная секция"
            className={classes.textField}
            value={localPlPlus}
            margin="normal"
            variant="outlined"
            multiline
            rows={37}
            fullWidth={true}
            propsButton={{ iconButtonChilds: () => <AssignmentIcon /> }}
          />
        </FormGroup>
      </Grid>
    </Grid>
  );
};
