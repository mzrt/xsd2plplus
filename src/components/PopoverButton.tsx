import React from "react";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Popover from "@material-ui/core/Popover";
import PopupState, { bindTrigger, bindPopover } from "material-ui-popup-state";
export interface PopoverButtonProps {
  children: any;
  title: any;
}
export default function PopoverButton(props: PopoverButtonProps): JSX.Element {
  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState: any): JSX.Element => (
        <div>
          <Button
            variant="contained"
            color="primary"
            {...bindTrigger(popupState)}
          >
            {props.children}
          </Button>
          <Popover
            {...bindPopover(popupState)}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center"
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center"
            }}
          >
            <Box p={2}>
              <Typography>{props.title}</Typography>
            </Box>
          </Popover>
        </div>
      )}
    </PopupState>
  );
}
