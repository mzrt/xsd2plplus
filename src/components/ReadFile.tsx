import React from "react";
import JSZip from "jszip";
class InnerFile {
  constructor(zipEntry: JSZip.JSZipObject, relativePath: string = "") {
    this.zipEntry = zipEntry;
    this.relativePath = relativePath;
  }
  zipEntry: JSZip.JSZipObject;
  relativePath: string = "";
}
class FileInfo {
  constructor(
    file: File,
    dateStart: Date = new Date(),
    contentFiles: InnerFile[] = []
  ) {
    this.file = file;
    this.dateStart = dateStart || new Date();
    this.contentFiles = contentFiles || [];
  }
  public file: File;
  public dateStart: Date;
  public dateFinish?: Date;
  public contentFiles: InnerFile[];
  public error: boolean = false;
  public errorMessage: string = "";
}

interface ReadFilesState {
  files: FileInfo[];
}
export class ReadFiles extends React.Component<never, ReadFilesState> {
  constructor(props: never) {
    super(props);
    this.state = {
      files: []
    };
  }

  // Closure to capture the file information.
  public handleFile = (fileInfo: FileInfo): void => {
    JSZip.loadAsync(fileInfo.file) // 1) read the Blob
      .then(
        (zip: JSZip) => {
          const updateFileInfo: FileInfo = {
            ...fileInfo,
            dateFinish: new Date(),
            contentFiles: []
          };

          zip.forEach(function (relativePath, zipEntry) {
            // 2) print entries
            updateFileInfo.contentFiles.push(
              new InnerFile(zipEntry, relativePath)
            );
          });
          this.setState((prevState) => ({
            files: prevState.files.map((fileItem) =>
              fileItem.file.name === fileInfo.file.name
                ? updateFileInfo
                : fileItem
            )
          }));
        },
        (e) => {
          const updateFileInfo: FileInfo = {
            ...fileInfo,
            error: true,
            errorMessage: e.message
          };
          this.setState((prevState) => ({
            files: prevState.files.map((fileItem) =>
              fileItem.file.name === fileInfo.file.name
                ? updateFileInfo
                : fileItem
            )
          }));
        }
      );
  };

  public onChangeFiles = ({
    target: { files: filesP }
  }: React.ChangeEvent<any>): void => {
    const files: FileInfo[] = [];
    for (let i = 0; i < filesP.length; i++) {
      const fileInfo: FileInfo = new FileInfo(filesP[i]);
      files.push(fileInfo);
    }
    files.forEach(this.handleFile);
    this.setState({ files });
  };
  render(): JSX.Element {
    const { files } = this.state;
    return (
      <div>
        <input
          type={"file"}
          id="file"
          name="file"
          multiple
          onChange={this.onChangeFiles}
        />
        <div>
          {files && files.length
            ? files.map(({ file, dateStart, dateFinish, contentFiles }) => (
                <div key={file.name}>
                  <h4>
                    {file.name}
                    {dateFinish ? (
                      <span className="small">
                        {" (loaded in " +
                          (dateFinish.getMilliseconds() -
                            dateStart.getMilliseconds()) +
                          "ms)"}
                      </span>
                    ) : null}
                  </h4>
                  <ul>
                    {contentFiles && contentFiles.length
                      ? contentFiles.map(({ zipEntry, relativePath }) => (
                          <li key={zipEntry.name + relativePath}>
                            {zipEntry.name + "(" + relativePath + ")"}
                          </li>
                        ))
                      : null}
                  </ul>
                </div>
              ))
            : null}
        </div>
      </div>
    );
  }
}
