import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Paper, Grid } from "@material-ui/core";
const changeLog = [
  {
    version: "0.0.16",
    date: new Date(2020, 0, 10),
    descr: [
      <span key="1">
        {
          "Исправил ошибку, возникающую когда последний элемент choice сложного типа."
        }
      </span>
    ]
  },
  {
    version: "0.0.15",
    date: new Date(2019, 9, 7),
    descr: [
      <span key="1">
        {
          "Исправил ошибку двоякой интерпритации реквизита сложного типа у которого в "
        }
      </span>,
      <span key="2">
        {
          "качесте имени типа указан тип данных совпадающий с именами простых типов "
        }
      </span>,
      <span key="3">
        {
          "данных, теперь простой тип данных будет игнорироваться если у тега есть дочерние теги.\n"
        }
      </span>,
      <span key="4">{' Восстановил вкладку "Настройки pl/plus".'}</span>,
      <span key="5">
        {
          " Вынес значения по-умолчанию в глобальный скрипт (js/defaultValues.js), чтобы можно было менять значения без пересборки проекта."
        }
      </span>
    ]
  },
  {
    version: "0.0.14",
    date: new Date(2019, 8, 22),
    descr: [
      <span key="1">
        {
          "Переписал скрипт на typescript. Отделил алгоритм построения иерархии строк от"
        }
      </span>,
      <span key="2">
        {
          " алгоритма формирования pl/plus-кода. Исправлена ошибка обработки вложенных"
        }
      </span>,
      <span key="3">{" структур с choice."}</span>
    ]
  },
  {
    version: "0.0.13",
    date: new Date(2018, 9, 15),
    descr: [
      <span key="1">
        {
          'Добавил возможность использования в качестве имени для структур pl/sql значения из колонки "Тип"'
        }
      </span>
    ]
  },
  {
    version: "0.0.12.01",
    date: new Date(2018, 9, 12),
    descr: [
      <span key="1">
        {
          'Поправил choice для случаев когда после "choice" идет текст вроде [0-1]'
        }
      </span>,
      <br key="2" />,
      <span key="">{"Добавил вывод даты в истории изменений"}</span>,
      <br key="3" />,
      <span key="4">{'Добавил вкладку "Иснтрукция по применению"'}</span>
    ]
  },
  {
    version: "0.0.12",
    date: new Date(2017, 3, 26),
    descr: [
      <span key="1">
        {"Для идентичных по структуре запросов, вместо комментария вида"}
      </span>,
      <br key="2" />,
      <span className="code_comment" key="comment">
        {
          "-- для чтения/записи запроса MoveReserveEKSRs используйте структуру AddInterestLegalLoanEKSRs_rec"
        }
      </span>,
      <br key="3" />,
      <span key="4">{"теперь создается тип строкой "}</span>,
      <br key="5" />,
      <span className="code_reservword" key="6">
        {"type "}
      </span>,
      <span className="code_identificator" key="7">
        {"MoveReserveEKSRs_rec "}
      </span>,
      <span className="code_reservword" key="8">
        {"is "}
      </span>,
      <span className="code_identificator" key="9">
        {"AddInterestLegalLoanEKSRs_rec"}
      </span>,
      <span className="code_oper" key="10">
        {";"}
      </span>,
      <br key="11" />,
      <span key="12">
        {
          'Также создаются "обертки-операции" для перевызова операций дублируемого типа'
        }
      </span>,
      <br key="13" />,
      <span key="14">
        {
          "Проверку на пустые значения теперь выполняется только для необязательных тегов, обязательные теги теперь выводятся даже при пустых значениях"
        }
      </span>
    ]
  },
  {
    version: "0.0.11",
    date: new Date(2017, 2, 2),
    descr: [
      <span key="1">
        {
          'Добавлено логирование в БД eksdev в справочник "Конвертер xsd. Журнал запросов" (работает только на серервере приложений)'
        }
      </span>,
      <br key="2" />,
      <span key="">
        {"Добавлены операции проверки структуры на пустые значения"}
      </span>,
      <br key="3" />,
      <span key="4">
        {
          "Добавлена проверка реквизита сложного типа на пустое значение, при пустом значении дочерних тегов, тег реквзиита не выводится"
        }
      </span>
    ]
  },
  {
    version: "0.0.10",
    date: new Date(2017, 2, 1),
    descr: [
      <span key="1">
        {
          "Добавлены шаблоны операций чтения/записи для корневых тегов отдельно от операций чтения/записи вложенных типов"
        }
      </span>,
      <br key="2" />,
      <span key="3">
        {
          'Добавлены шаблоны "Запись сложного типа", "Чтения сложного типа" для обрамления шаблонов "Чтение элемента сложного типа", "Запись элемента сложного типа", "Чтение таблицы", "Запись таблицы"'
        }
      </span>,
      <br key="4" />,
      <span key="">
        {
          "Переработаны шаблоны для чтения/записи сложных типов и сложных табличных типов"
        }
      </span>,
      <br key="5" />,
      <span key="6">
        {
          'Переработаны шаблоны операций чтения/записи, вывод "обрамляющего" тега вынесен за пределы операции'
        }
      </span>
    ]
  },
  {
    version: "0.0.9",
    date: new Date(2017, 1, 27),
    descr: [
      <span key="1">
        {
          "Обновлен список зарезервированных слов Pl/Plus, список взят из документации Pl/Plus от 12.09.2011"
        }
      </span>
    ]
  },
  {
    version: "0.0.8",
    date: new Date(2017, 1, 27),
    descr: [
      <span key="2">
        {
          "Обновил текст для примера, взял текст полученный на основе схемы на сайте "
        }
      </span>,
      <br key="3" />,
      <span key="4">
        {
          'До этого использовался текст отредактированный пользователем, некорректно обрабатывались строки, у которых в колонке "Описание" содержались символы переводы строки'
        }
      </span>
    ]
  },
  {
    version: "0.0.7",
    date: new Date(2017, 1, 22),
    descr: [
      <span key="5">
        {
          'Добавлена обработка данных вместе с шапками таблиц(текст "Элемент Тип Описание Кратность") и текстом между таблицами'
        }
      </span>,
      <br key="6" />,
      <span key="7">
        {
          "Удалил встроенную операцию логирования в файл, заменил на использование макроса из tlib - &pl"
        }
      </span>
    ]
  },
  {
    version: "0.0.6",
    date: new Date(2017, 1, 21),
    descr: [
      <span key="8">
        {
          "Для дублирующихся типов первого уровня, добавляются комментарии со ссылкой на тип который следует использовать"
        }
      </span>
    ]
  },
  {
    version: "0.0.5",
    date: new Date(2017, 1, 21),
    descr: [<span key="">{"Исправлено зацикливание в макросе"}</span>]
  },
  {
    version: "0.0.4",
    date: new Date(2017, 1, 21),
    descr: [
      <p key="1">
        {
          'Добавил список зарезервированных слов, зарезервированные слова берутся в "".'
        }
      </p>,
      <br key="2" />,
      <p key="3">{"Добавил вывод &out_ini в корневом теге."}</p>,
      <br key="4" />,
      <span key="5">
        {"Добавил генерацию w_, r_ по маске корневого тега."}
      </span>,
      <br key="6" />,
      <span key="7">{"Добавил вывод лога в файл"}</span>
    ]
  },
  {
    version: "0.0.1",
    date: new Date(2017, 1, 20),
    descr: [
      <span key="1">
        {'Добавил флаг "Генерировать" для шаблонов операций чтения и записи'}
      </span>
    ]
  }
];
export const versionScript = changeLog[0].version;

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      flexWrap: "wrap"
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    paper: {
      padding: theme.spacing(2),
      color: theme.palette.text.secondary,
      marginBottom: theme.spacing(1)
    },
    button: {
      margin: theme.spacing(1)
    }
  })
);
const Help = (): JSX.Element => {
  const classes = useStyles();
  return (
    <Grid container spacing={2} className={classes.container}>
      {changeLog.map(
        (elem): JSX.Element => {
          const dateStr =
            (elem.date.getDate() < 10 ? "0" : "") +
            elem.date.getDate() +
            "." +
            (elem.date.getMonth() < 9 ? "0" : "") +
            (elem.date.getMonth() + 1) +
            "." +
            elem.date.getFullYear();
          return (
            <Grid item xs={12} key={elem.version}>
              <Paper className={classes.paper}>
                <Grid container key="header" className={classes.container}>
                  <Grid item sm={10} xs={12} key="version">
                    {elem.version}
                  </Grid>
                  <Grid item sm={2} xs={12} key="date">
                    {dateStr}
                  </Grid>
                </Grid>
              </Paper>
              <Paper className={classes.paper}>
                <Grid
                  container
                  key="description"
                  className={classes.container}
                  spacing={2}
                >
                  <Grid item xs={12}>
                    {elem.descr}
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          );
        }
      )}
    </Grid>
  );
};
export default Help;
