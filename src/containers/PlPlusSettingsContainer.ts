import { connect } from "react-redux";
import { AppState } from "../reducers";
import {
  setPropDataTypeTemplates,
  setProp,
  setPropTemplates,
  SettingsProps
} from "../actions";
import { TemplatesProps } from "../actions/templates";
import { Templates } from "../utils/templates";
import { GeneratorSettings } from "../reducers/GeneratorSettings";
import { DataTypeTemplatesProps } from "../actions/dataTypeTemplates";
import { DataTypeTemplates } from "../utils/TemplatesDataType";
import {
  PlPlusGeneratorOptions,
  PlPlusSettingsProps
} from "../components/PlPlusSettings";
const mapStateToProps = (
  state: AppState
): { [K in keyof PlPlusSettingsProps]?: PlPlusSettingsProps[K] } => {
  const { settings } = state;
  return {
    settings
  };
};
export default connect(mapStateToProps, (dispatch) => ({
  onChangeDataTypeTemplate: <T extends DataTypeTemplatesProps>(prop: T) => (
    value: DataTypeTemplates[T]
  ): void => {
    dispatch(setPropDataTypeTemplates(prop)(value));
  },
  onChangeSettings: <T extends SettingsProps>(prop: T) => (
    value: GeneratorSettings[T]
  ): void => {
    dispatch(setProp(prop)(value));
  },
  onChangeTemplate: <T extends TemplatesProps>(prop: T) => (
    value: Templates[T]
  ): void => {
    dispatch(setPropTemplates(prop)(value));
  }
}))(PlPlusGeneratorOptions);
