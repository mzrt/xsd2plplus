import { connect } from "react-redux";
import { AppState } from "../reducers";
import {
  addFile,
  removeFile,
  changeFile,
  clearFiles,
  setFiles,
  setStartFile
} from "../actions";

import {
  ReadXsdTextInput,
  ReadXsdTextInputProps,
  ReadXsdTextInputPropsFunc
} from "../components/ReadXsdTextInput";
const mapStateToProps = (state: AppState): ReadXsdTextInputProps => {
  const { xsdFiles: files, xsdStartFile: startXsd } = state;
  return {
    files,
    startXsd
  };
};
export default connect(mapStateToProps, {
  addFile,
  removeFile,
  changeFile,
  clearFiles,
  setFiles,
  setStartXsdFile: setStartFile
} as ReadXsdTextInputPropsFunc)(ReadXsdTextInput);
