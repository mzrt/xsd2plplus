import App, { AppProps } from "../App";
import { connect } from "react-redux";
import { AppState } from "../reducers";
import { setProp, setPropTemplates } from "../actions";
import { TemplatesProps } from "../actions/templates";
import { Templates } from "../utils/templates";
const mapStateToProps = (
  state: AppState
): { [K in keyof AppProps]?: AppProps[K] } => {
  const { settings, xsdStartFile: startXsd, xsdFiles } = state;
  return {
    settings,
    startXsd,
    xsdFiles: xsdFiles || []
  };
};
export default connect(mapStateToProps, (dispatch) => ({
  onChangeXsdTbl: (val: string): void => {
    dispatch(setProp("tbl")(val));
  },
  onChangeTemplate: <T extends TemplatesProps>(prop: T) => (
    value: Templates[T]
  ): void => {
    dispatch(setPropTemplates(prop)(value));
  }
}))(App);
