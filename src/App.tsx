import React, { ChangeEvent } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Help from "./components/Help";
import ChangeLog from "./components/ChangeLog";
import {
  GeneratorOptions,
  ApplyMaskState
} from "./components/GeneratorOptions";
import { GeneratorSettings } from "./reducers/GeneratorSettings";
import { AppBar, Button, Grid } from "@material-ui/core";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { PlPlusResult } from "./components/PlPlusResult";
import { SrcCodes } from "./utils/PlPlusGenerator";
import { defaultTemplates, Templates } from "./utils/templates";
import { defaultDataTypeTemplates } from "./utils/TemplatesDataType";
import PlPlusGeneratorOptions from "./containers/PlPlusSettingsContainer";
import { TemplatesProps } from "./actions/templates";
import { ReadFiles } from "./components/readFiles";
import JSZip from "jszip";
import { FileContentInfo, FileInfo, InnerFile } from "./models";
import { unzipBlob } from "./utils/zip";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Radio from "@material-ui/core/Radio/Radio";
import ReadXsdTextInputContainer from "./containers/ReadXsdTextInputContainer";
// import { Xsd2JsonSchema } from "xsd2jsonschema";
import {
  // xml2xsd,
  xsd2jsonSchema
  // json2xsd,
  // validateXml,
  // detectXmlSchema,
  // jsonSchema2xsd
} from "xsdlibrary";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const TabContainer = (props: any): JSX.Element => {
  return (
    <React.Fragment>
      <div>
        <Tabs value={1}></Tabs>
      </div>
      <Typography component="div" style={{ padding: 8 * 2 }}>
        {props.children}
      </Typography>
    </React.Fragment>
  );
};
export interface AppProps {
  settings?: GeneratorSettings;
  startXsd?: string;
  xsdFiles?: FileContentInfo[];
  onChangeXsdTbl: (value: string) => void;
  onChangeTemplate: <T extends TemplatesProps>(
    prop: T
  ) => (value: Templates[T]) => void;
}
type XsdSource = "xsdZipFile" | "textInput";
export interface AppState {
  tabValue: number;
  mask1: ApplyMaskState;
  mask2: ApplyMaskState;
  plplusGlobal: string;
  plplusLocal: string;
  files: FileInfo[];
  filesContent: FileContentInfo[][];
  startXsd: (string | undefined)[];
  xsdSource: XsdSource;
}
class App extends React.Component<AppProps, AppState> {
  public constructor(props: AppProps) {
    super(props);
    const {
      settings: { rqMask, rsMask } = new GeneratorSettings()
    } = this.props;
    this.state = {
      tabValue: 0,
      mask1: {
        apply: false,
        mask: rqMask,
        forRead: false,
        forWrite: true
      },
      mask2: {
        apply: false,
        mask: rsMask,
        forRead: true,
        forWrite: false
      },
      plplusGlobal: "",
      plplusLocal: "",
      files: [],
      filesContent: [],
      startXsd: [],
      xsdSource: "xsdZipFile"
    };
  }

  public getNewFileContentInfo = (
    prevState: FileContentInfo[][],
    idxZip: number,
    innerFileIdx: number,
    value: FileContentInfo
  ): FileContentInfo[][] => {
    return prevState.map((_currentZip, currentZipIdx) => {
      const currentContent = prevState[currentZipIdx] || [];
      if (currentZipIdx !== idxZip) {
        return currentContent;
      } else {
        const newContent = currentContent.slice(0);
        newContent[innerFileIdx] = value;
        return newContent;
      }
    });
  };
  public setFileContent = (idxZip: number) => (
    unzippedBlob: any,
    unzippedContentIdx?: number
  ): void => {
    if (
      unzippedContentIdx === 0 ||
      (unzippedContentIdx && unzippedContentIdx > -1)
    ) {
      const itemReader = new FileReader();
      this.setState((prevState) => ({
        filesContent: this.getNewFileContentInfo(
          prevState.filesContent,
          idxZip,
          unzippedContentIdx,
          {
            ...prevState.filesContent[idxZip][unzippedContentIdx],
            status: "loading",
            text: ""
          }
        )
      }));
      // This fires after the blob has been read/loaded.
      itemReader.addEventListener("loadend", (e: any) => {
        const text = e && e.srcElement && e.srcElement.result;
        console.log(unzippedContentIdx, text);
        this.setState((prevState) => ({
          filesContent: this.getNewFileContentInfo(
            prevState.filesContent,
            idxZip,
            unzippedContentIdx,
            {
              ...prevState.filesContent[idxZip][unzippedContentIdx],
              status: "ready",
              text,
              open: false
            }
          )
        }));
      });
      // Start reading the blob as text.
      itemReader.readAsText(unzippedBlob);
    }
  };
  public setStartXsdFile = (zipIdx: number, entryIdx: string): void => {
    this.setState((prevState) => ({
      startXsd: prevState.startXsd.map((item, itemIdx) =>
        itemIdx === zipIdx ? entryIdx : item
      )
    }));
  };
  // Closure to capture the file information.
  public handleFile = (fileInfo: FileInfo): void => {
    JSZip.loadAsync(fileInfo.file) // 1) read the Blob
      .then(
        (zip: JSZip) => {
          const updateFileInfo: FileInfo = {
            ...fileInfo,
            dateFinish: new Date(),
            contentFiles: []
          };

          zip.forEach(function (relativePath, zipEntry) {
            // 2) print entries
            updateFileInfo.contentFiles.push(
              new InnerFile(zipEntry, relativePath)
            );
          });
          this.setState((prevState) => {
            return {
              files: prevState.files.map((fileItem) =>
                fileItem.file.name === fileInfo.file.name
                  ? updateFileInfo
                  : fileItem
              ),
              filesContent: prevState.files.map((fileItem, itemIdx: number) =>
                fileItem.file.name === fileInfo.file.name
                  ? updateFileInfo.contentFiles.map((item) => ({
                      status: "unknow",
                      text: "",
                      relativePath: item.relativePath,
                      filename: item.zipEntry.name
                    }))
                  : prevState.filesContent[itemIdx] || []
              ),
              startXsd: prevState.files.map((fileItem, itemIdx: number) =>
                fileItem.file.name === fileInfo.file.name
                  ? ""
                  : prevState.startXsd[itemIdx]
              )
            };
          });
        },
        (e) => {
          const updateFileInfo: FileInfo = {
            ...fileInfo,
            error: true,
            errorMessage: e.message
          };
          this.setState((prevState) => ({
            files: prevState.files.map((fileItem) =>
              fileItem.file.name === fileInfo.file.name
                ? updateFileInfo
                : fileItem
            )
          }));
        }
      )
      .then(() => {
        this.state.files.forEach((fileState, idxZip) => {
          if (
            fileState &&
            !fileState.error &&
            fileState.file.name === fileInfo.file.name
          ) {
            // unzip the first file from zipped data stored in zippedBlob
            unzipBlob(fileInfo.file, this.setFileContent(idxZip));
          }
        });
      });
  };

  public onChangeFiles = ({
    target: { files: filesP }
  }: React.ChangeEvent<any>): void => {
    const files: FileInfo[] = [];
    for (let i = 0; i < filesP.length; i++) {
      const fileInfo: FileInfo = new FileInfo(filesP[i]);
      files.push(fileInfo);
    }
    files.forEach(this.handleFile);
    this.setState({ files });
  };

  handleChange = (event: any, tabValue: number): void => {
    this.setState({ tabValue });
  };

  handleChangeMask = (
    maskName: "mask1" | "mask2"
  ): ((maskValue: ApplyMaskState) => void) => (
    maskValue: ApplyMaskState
  ): void => {
    if (maskName === "mask1") {
      this.setState({ mask1: maskValue });
    }
    if (maskName === "mask2") {
      this.setState({ mask2: maskValue });
    }
  };
  public runGenerator = (): void => {
    const {
      settings: {
        genRead,
        genWrite,
        tbl,
        templates,
        dataTypeTemplates,
        reservWords
      } = {
        genRead: true,
        genWrite: true,
        tbl: "",
        templates: defaultTemplates,
        dataTypeTemplates: defaultDataTypeTemplates,
        reservWords: ""
      }
    } = this.props;
    const { mask1, mask2 } = this.state;
    const srcCode: SrcCodes = new SrcCodes(
      tbl,
      templates,
      dataTypeTemplates,
      mask1,
      mask2,
      reservWords,
      genRead,
      genWrite
    );
    this.setState({
      tabValue: 2,
      plplusGlobal: srcCode.global(),
      plplusLocal: srcCode.local()
    });
  };
  public handleChangeXsdSource = (
    event: ChangeEvent<HTMLInputElement>,
    checked: boolean
  ): void => {
    this.setState({
      xsdSource: (event.target as HTMLInputElement).value as XsdSource
    });
  };
  public runXsdProcess = (): void => {
    const { xsdFiles, startXsd: inputTextFilesStart } = this.props;
    const { filesContent, startXsd, xsdSource } = this.state;
    const schemas: any = {};
    let startSchema: string = "";
    if (xsdSource !== "xsdZipFile" && xsdFiles) {
      xsdFiles.forEach((item, indexFile) => {
        if (item.relativePath) {
          schemas[item.relativePath] = item.text;
          if (
            inputTextFilesStart &&
            inputTextFilesStart === String(indexFile)
          ) {
            startSchema = item.relativePath;
          }
        }
      });
    }
    if (
      xsdSource === "xsdZipFile" &&
      filesContent[0] &&
      filesContent[0].length
    ) {
      const zipFileIndex =
        (startXsd.length !== 0 && startXsd[0] !== undefined) || startXsd[0];
      filesContent[0].forEach((item, indexFile) => {
        if (item.relativePath) {
          schemas[item.relativePath] = item.text;
          if (zipFileIndex && zipFileIndex === String(indexFile)) {
            startSchema = item.relativePath;
          }
        }
      });
    }
    const jsonSchema =
      startSchema && schemas[startSchema]
        ? xsd2jsonSchema(schemas[startSchema])
        : null;
    console.log(jsonSchema);
  };
  render(): JSX.Element {
    const { settings = new GeneratorSettings(), onChangeXsdTbl } = this.props;
    const { startXsd: inputTextFilesStart } = this.props;
    const {
      plplusGlobal,
      plplusLocal,
      tabValue,
      mask1,
      mask2,
      files,
      filesContent,
      startXsd,
      xsdSource
    } = this.state;
    return (
      <React.Fragment>
        <CssBaseline />
        <div>
          <AppBar position="fixed" color="default">
            <Tabs
              value={tabValue}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
              scrollButtons="auto"
            >
              <Tab label="XSD в виде таблицы" />
              <Tab label="XSD" />
              <Tab label="Результат" />
              <Tab label="Настройки pl/plus" />
              <Tab label="Справка" />
              <Tab label="История изменений" />
            </Tabs>
          </AppBar>
          {tabValue === 0 && (
            <TabContainer key="txtTable">
              <GeneratorOptions
                settings={settings}
                onChangeXsdTbl={onChangeXsdTbl}
                onChangeMask1={this.handleChangeMask("mask1")}
                onChangeMask2={this.handleChangeMask("mask2")}
                mask1={mask1}
                mask2={mask2}
                runGenerator={this.runGenerator}
              />
            </TabContainer>
          )}
          {tabValue === 1 && (
            <TabContainer key="xsd">
              <Grid container spacing={2}>
                <Grid item wrap="nowrap" xs={12}>
                  {[
                    { src: "xsdZipFile", title: "Из zip-архива" },
                    { src: "textInput", title: "Из текстовых полей" }
                  ].map(({ src: itemSrc, title: itemTitle }) => (
                    <FormControlLabel
                      value={itemSrc}
                      control={
                        <Radio
                          checked={xsdSource === itemSrc}
                          onChange={this.handleChangeXsdSource}
                          value={itemSrc}
                          color="default"
                          name="radio-button-demo"
                          inputProps={{ "aria-label": "D" }}
                        />
                      }
                      label={itemTitle}
                    />
                  ))}
                </Grid>
                <Grid item xs={12}>
                  {xsdSource === "xsdZipFile" ? (
                    <ReadFiles
                      files={files}
                      filesContent={filesContent}
                      onChangeFiles={this.onChangeFiles}
                      setStartXsdFile={this.setStartXsdFile}
                      startXsd={startXsd}
                    />
                  ) : (
                    <ReadXsdTextInputContainer />
                  )}
                </Grid>
                <Grid item xs={12}>
                  <Button
                    variant="contained"
                    disabled={
                      xsdSource === "xsdZipFile"
                        ? startXsd.length === 0 ||
                          startXsd[0] === undefined ||
                          startXsd[0] === ""
                        : !inputTextFilesStart?.length
                    }
                    onClick={this.runXsdProcess}
                  >
                    Запустить
                  </Button>
                </Grid>
              </Grid>
            </TabContainer>
          )}
          {tabValue === 2 && (
            <TabContainer key="plPlus">
              <PlPlusResult
                globalPlPlus={plplusGlobal}
                localPlPlus={plplusLocal}
              />
            </TabContainer>
          )}
          {tabValue === 3 && (
            <TabContainer key="plplusgeneratorOptions">
              <PlPlusGeneratorOptions />
            </TabContainer>
          )}
          {tabValue === 4 && (
            <TabContainer key="help">
              <Help />
            </TabContainer>
          )}
          {tabValue === 5 && (
            <TabContainer key="changelog">
              <ChangeLog />
            </TabContainer>
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default App;
